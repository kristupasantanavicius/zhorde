#include <Game/Level.h>
#include <Game/SpriteBits.h>
#include <Game/Weapon.h>
#include <Game/DebugManifest.h>
#include <Game/CircleRender.h>
#include <Game/FrameEffect.h>
#include <Game/MoneyEffect.h>
#include <Game/ZombieConfig.h>
#include <Game/Console.h>
#include <minigine/KeyboardInput.h>
#include <minigine/Random.h>
#include <minigine/Asserts.h>
#include <minigine/utils/Math.h>
#include <minigine/ImageVO.h>
#include <minigine/ColorPadVO.h>
#include <minigine/SpriteBatch.h>
#include <minigine/utils/Math.h>
#include <algorithm>
#include <limits>

glm::vec2 tileSize(48, 48);
glm::vec2 entSize(48, 48);

float randomNextFloat(const glm::vec2 & vec) {
	return randomNextFloat(vec.x, vec.y);
}

int randomNextInt(const glm::vec2 & vec) {
	return randomNextInt((int)vec.x, (int)vec.y);
}

float stunToVelocityScale(float stun)
{
	float normal = Minigine::Math::cap(0.0f, 1.0f, stun / 5.0f);
	return 1 - normal; // Invert; 0 stun means no velocity penalty.
}

void stepStunValue(float & stun, float fd) {
	if (stun > 0) {
		stun -= fd * 10.0f;
	}
	if (stun > 5) {
		stun = 5;
	}
}

float lerpVector2(const glm::vec2 & vec, float normal) {
	return vec.x + (vec.y - vec.x) * normal;
}

Level::Level(SPInventory inventory, SPTimeline timeline, LevelCallback callback)
: mCamera(Camera(Minigine::getScreenSize()))
, mCallback(callback)
, mInventory(inventory)
, mTimeline(timeline)
{
	Minigine::SPImage levelImage = Minigine::Image::fromFile("resources/textures/level.png", Minigine::Image::eFlipVertically);
	mWidth = levelImage->getWidth();
	mHeight = levelImage->getHeight();

	MINIGINE_ASSERT(std::numeric_limits<SpawnIndex_t>::max() >= mWidth);
	MINIGINE_ASSERT(std::numeric_limits<SpawnIndex_t>::max() >= mHeight);

	mTiles.resize(mWidth * mHeight);
	int stride = levelImage->getStride();
	for (int y = 0; y < mHeight; y++) {
		for (int x = 0; x < mWidth; x++) {
			unsigned char pixel = levelImage->getData()[(x + y * mWidth) * 4];
			if (pixel == 255) {
				mTiles.at(x + y * mWidth) = 1;
			}
			else if (pixel == 64) {
				mTiles.at(x + y * mWidth) = 0;
				mSpawnIndexes.push_back( { SpawnIndex_t(x), SpawnIndex_t(y) } );
			}
			else MINIGINE_BREAK("");
		}
	}

	mBoundsMin = glm::vec2();
	mBoundsMax = glm::vec2(mWidth, mHeight) * tileSize;

	auto player = spawnEntity<Player>();
	mPlayerHandle = player->getEntityHandle();
	player->getImage()->setAlignment(0.5f, 0.5f);
	player->setDiameter(entSize.x);
	player->entityMove(tileSize * 3.0f + tileSize * 0.5f);

	mSoundZombieDeath = Minigine::Sound::create("resources/sounds/zombiedeath.wav", false);
	mSoundZombieDeath->setVolume(0.25f);

	refreshWeapon();

	changeZombieConfig(ZombieConfigs::inst()->getConfigForTime(0));

	mLaser.reset(new Minigine::ColorPadVO(glm::vec4(1, 0, 1, 0.4f), glm::vec2(2000.0f, 3.0f)));
	mLaser->setAlignment(0.0f, 0.5f);
}

void
Level::update(float fd)
{
	if (mPaused) {
		return;
	}

	{
		mLevelTime += fd;
		mTimeline->setLevelTime(mLevelTime);
		const ZombieConfig * config = ZombieConfigs::inst()->getConfigForTime(mLevelTime);
		if (config != mZombieConfig) {
			changeZombieConfig(config);
		}
		mCallback.cSetLevelTime(mLevelTime);
	}

	{
		auto * player = getPlayer();
		glm::vec2 velocity;
		if (KeyboardInput::instance().isKeyDown(KeyboardInput::KEY_A)) {
			velocity.x -= 1.0f;
		}
		if (KeyboardInput::instance().isKeyDown(KeyboardInput::KEY_D)) {
			velocity.x += 1.0f;
		}
		if (KeyboardInput::instance().isKeyDown(KeyboardInput::KEY_W)) {
			velocity.y += 1.0f;
		}
		if (KeyboardInput::instance().isKeyDown(KeyboardInput::KEY_S)) {
			velocity.y -= 1.0f;
		}

		velocity *= 200.0f;

		// Scale each axis separately, so that they don't become floating point numbers.
		float stunScale = stunToVelocityScale(player->mPlayerStun);
		velocity.x *= stunScale;
		velocity.y *= stunScale;

		stepStunValue(player->mPlayerStun, fd);
		mPlayerHUD.setStun(player->mPlayerStun, 5.0f);

		glm::vec2 oldPosition = player->getPosition();
		glm::vec2 newPosition = oldPosition + velocity * fd;

		// So that its possible to move on 1 axis while the other one is blocked.
		if (circleCollidesWithAnyTile(glm::vec2(newPosition.x, oldPosition.y), player->mRadius)) {
			newPosition.x = oldPosition.x;
		}
		if (circleCollidesWithAnyTile(glm::vec2(oldPosition.x, newPosition.y), player->mRadius)) {
			newPosition.y = oldPosition.y;
		}

		player->entityMove(newPosition);

		mPlayerPosition = player->getPosition();
		mCamera.mOffset = mPlayerPosition;
		mCamera.mOffset.x = (float)(int)mCamera.mOffset.x;
		mCamera.mOffset.y = (float)(int)mCamera.mOffset.y;

		glm::vec2 pointerOnMap = mCamera.convertScreenToWorld(mPointerPosition);
		glm::vec2 delta = pointerOnMap - mPlayerPosition;
		float rotation = atan2f(delta.y, delta.x);
		player->entityRotate(rotation);

		glm::vec2 barrelVec = Minigine::rotateVector(getWeaponConfig()->mBarrelPoint, getPlayer()->getRotationAdjusted());
		mLaser->setPosition(mPlayerPosition + barrelVec);
		mLaser->setRotation(Minigine::toDegree(getPlayer()->getRotation()));

		mPlayerHUD.setPosition(player->getPosition());
		mPlayerHUD.update(fd);
	}

	checkZombieSpawn(fd);
	checkFiring(fd);
	updateReloading(fd);

	int numZombiesCollideWithPlayer = 0;

	auto player = getPlayer();
	for (auto & entity : mEntities) {
		if (auto bullet = toBullet(entity)) {
			float speed = bullet->mBulletSpeed * fd;
			float rotation = bullet->getRotation();
			glm::vec2 deltaVec(cosf(rotation) * speed, sinf(rotation) * speed);
			bullet->entityMove(bullet->getPosition() + deltaVec);

			glm::vec2 bulletPos = bullet->getPosition();
			bool removed = circleCollidesWithAnyTile(bulletPos, bullet->mRadius)
				|| bulletPos.x < mBoundsMin.x || bulletPos.x > mBoundsMax.x
				|| bulletPos.y < mBoundsMin.y || bulletPos.y > mBoundsMax.y;
			if (!entity->isRemoved() && removed) {
				SPFrameEffect effect;
				effect.reset(new FrameEffect(0.03f, glm::vec2(32, 32), glm::vec2(0.5f, 0.5f), bullet->getRotationAdjusted(), SpriteBits::inst()->Effect_Bullet_Wall));
				effect->setPosition(bullet->getPosition());
				mEffects.push_back(effect);

				removeEntity(bullet->getEntityHandle());
			}
		}
		else if (auto zombie = toZombie(entity)) {
			updateZombie(zombie, fd);
			checkBulletCollisions(zombie);

			if (entitiesCollide(zombie, player)) {
				numZombiesCollideWithPlayer += 1;
			}
		}
	}

	if (numZombiesCollideWithPlayer > 0) {
		float stunValue = 10.0f;
		stunValue += 1.0f * numZombiesCollideWithPlayer;
		player->mPlayerStun += stunValue * fd;
	}

	for (auto & handle : mRemovedEntities) {
		destroyEntityInline(handle);
	}
	mRemovedEntities.clear();

	for (auto iter = mEffects.begin(); iter != mEffects.end(); /*EMPTY*/ ) {
		const SPFrameEffect & effect = *iter;
		if (effect->update(fd)) {
			iter = mEffects.erase(iter);
		}
		else {
			++iter;
		}
	}

	for (auto iter = mMoneyEffects.begin(); iter != mMoneyEffects.end(); /*EMPTY*/ ) {
		const SPMoneyEffect & moneyEffect = *iter;
		if (moneyEffect->update(fd, mPlayerPosition)) {
			mInventory->addMoney(moneyEffect->mMoney);
			iter = mMoneyEffects.erase(iter);
		}
		else {
			++iter;
		}
	}
}

void
Level::render(const Minigine::DrawInfo & parentDrawInfo)
{
	Minigine::DrawInfo drawInfo = parentDrawInfo;
	drawInfo.transform = mCamera.getCameraRenderTransform();

	Minigine::ImageVO tileImage;
	for (int y = 0; y < mHeight; y++) {
		for (int x = 0; x < mWidth; x++) {
			switch (mTiles.at(x + y * mWidth)) {
				case 0: tileImage.setSprite(SpriteBits::inst()->Tile_Grass, tileSize); tileImage.setColor(glm::vec4(0.3f, 0.7f, 0.3f, 1)); break;
				case 1: tileImage.setSprite(SpriteBits::inst()->Tile_Rock, tileSize); tileImage.setColor(glm::vec4(1, 1, 1, 1)); break;
				default: continue;
			}
			tileImage.setPosition(glm::vec2(x, y) * tileSize);
			tileImage.draw(drawInfo);
		}
	}

	for (auto & entity : mEntities) {
		entity->render(drawInfo);
		
		if (auto player = toPlayer(entity)) {
			glm::vec2 offsetRotated = Minigine::rotateVector(getWeaponConfig()->mOffset, player->getRotationAdjusted());
			player->mGun.setPosition(player->getPosition() + offsetRotated);
			player->mGun.setRotation(glm::degrees(player->getRotationAdjusted()));
			player->mGun.draw(drawInfo);

			mPlayerHUD.draw(drawInfo);
		}

#if 0
		if (DebugManifest::inst()->displayCollisionMeshes() && entity->has<CompRadius>()) {
			CircleRender::draw(drawInfo, entity->getPosition(), entity->get<CompRadius>()->mRadius, glm::vec4(0, 1, 1, 1));
		}
#endif
	}

	for (auto & effect : mEffects) {
		effect->draw(drawInfo);
	}

	for (auto & moneyEffect : mMoneyEffects) {
		moneyEffect->draw(drawInfo);
	}

	mLaser->draw(drawInfo);

#if 0
{
	float rayDistance = 0.0f;
	bool visible = raycast(mPlayerPosition, mCamera.convertScreenToWorld(mPointerPosition), &rayDistance);
	glm::vec4 color = glm::vec4(0, 1, 0, 1);
	if (!visible) {
		color = glm::vec4(1, 0, 0, 1);
	}
	Minigine::ColorPadVO colorpad(color, glm::vec2(rayDistance, 2.0f));
	colorpad.setAlignment(0, 0.5f);
	colorpad.setPosition(mPlayerPosition);
	colorpad.setRotation(glm::degrees(getPlayer()->getRotation()));
	colorpad.draw(drawInfo);
}
#endif
#if 0
{
	glm::vec2 screenSize = Minigine::getScreenSize();
	int totalRays = 2000;
	float screenRatio = screenSize.x / screenSize.y;
	int raysHorinzontal = (int) (totalRays * (screenRatio / (screenRatio+1.0f)));
	int raysVertical = totalRays - raysHorinzontal;
	int raysPerSideHorizontal = raysHorinzontal / 2;
	int raysPerSideVertical = raysVertical / 2;
	std::vector<glm::vec2> screenPoints;
	for (int i = 0; i < raysPerSideHorizontal; i++) {
		float x = screenSize.x / raysPerSideHorizontal * i;
		screenPoints.push_back(glm::vec2(x, 0));
	}
	for (int i = 0; i < raysPerSideVertical; i++) {
		float y = screenSize.y / raysPerSideVertical * i;
		screenPoints.push_back(glm::vec2(screenSize.x, y));
	}
	for (int i = 0; i < raysPerSideHorizontal; i++) {
		float x = screenSize.x / raysPerSideHorizontal * i;
		screenPoints.push_back(glm::vec2(screenSize.x - x, screenSize.y));
	}
	for (int i = 0; i < raysPerSideVertical; i++) {
		float y = screenSize.y / raysPerSideVertical * i;
		screenPoints.push_back(glm::vec2(0, screenSize.y - y));
	}

	struct MeshPoint {
		glm::vec2 mOrigin;
		glm::vec2 mCollision;
		bool mVisible = false;
	};

	std::vector<MeshPoint> meshPoints;
	meshPoints.reserve(screenPoints.size());
	for (auto & screenPoint : screenPoints) {
		MeshPoint point;
		point.mOrigin = mCamera.convertScreenToWorld(screenPoint);

		float rayDistance = 0.0f;
		point.mVisible = raycast(mPlayerPosition, point.mOrigin, &rayDistance);

		glm::vec2 angleVec = glm::normalize(point.mOrigin - mPlayerPosition);
		point.mCollision = mPlayerPosition + angleVec * rayDistance;

		meshPoints.push_back(point);

#if 0
		glm::vec4 color = glm::vec4(0, 1, 0, 0.2f);
		if (!point.mVisible) {
			color = glm::vec4(1, 0, 0, 0.2f);
		}

		Minigine::ColorPadVO colorpad(color, glm::vec2(rayDistance, 1.0f));
		colorpad.setAlignment(0, 0.5f);
		colorpad.setPosition(mPlayerPosition);

		colorpad.setRotation(glm::degrees(atan2f(angleVec.y, angleVec.x)));
		colorpad.draw(drawInfo);
#endif
	}

	Minigine::SpriteBatch batch;
	for (int i = 0; i < meshPoints.size() - 1; i++) {
		const MeshPoint & left = meshPoints.at(i);
		const MeshPoint & right = meshPoints.at(i + 1);
		Minigine::SpriteBatch::Vertex v1;
		Minigine::SpriteBatch::Vertex v2;
		Minigine::SpriteBatch::Vertex v3;
		Minigine::SpriteBatch::Vertex v4;
		v1.position = left.mOrigin;
		v2.position = left.mCollision;
		v3.position = right.mCollision;
		v4.position = right.mOrigin;
		batch.addVertices(v1, v2, v3, v4, glm::vec4(1, 1, 1, 1.0f));
	}
	batch.draw(drawInfo);
}
#endif
}

Minigine::VisibleObject2d::TouchHandleResult
Level::pointerEvent(const Minigine::PointerEvent & event)
{
	if (event.hasPrimaryPointerData()) {
		mPointerPosition = glm::vec2(event.getPrimaryPointerX(), event.getPrimaryPointerY());
	}

	if (event.hasPrimaryPointerData()) {
		if (event.isDown()) {
			mFiring = true;
		}
		else if (event.isUp()) {
			mFiring = false;
		}
	}

	return Minigine::VisibleObject2d::eNotMine;
}

void
Level::keyPressed(int key)
{
	if (key == KeyboardInput::KEY_Q) {
		mInventory->selectNextWeapon();
		refreshWeapon();
	}
	else if (key == KeyboardInput::KEY_E) {
		mInventory->selectPreviousWeapon();
		refreshWeapon();
	}
	else if (key == KeyboardInput::KEY_R) {
		if (!isReloading() && !mInventory->magazineIsFull() && mInventory->hasAmmoInBackpack()) {
			beginReloading();
		}
	}
}

void
Level::setLevelPaused(bool paused)
{
	mPaused = paused;
}

void
Level::checkFiring(float fd)
{
	bool shouldFire = false;
	if (mFiring) {
		if (getWeaponConfig()->mAutoFire) {
			shouldFire = true;
		}
		else if (!mWasFiring) {
			shouldFire = true;
		}
	}

	mWasFiring = mFiring;

	mWeaponFireDelay -= fd;

	if (shouldFire == false) {
		return;
	}

	if (mWeaponFireDelay >= 0.0f) {
		shouldFire = false;
	}

	if (shouldFire && isReloading()) {
		shouldFire = false;
	}

	if (shouldFire && mInventory->magazineIsOutOfAmmo()) {
		// TODO: Play sound for empty gun.
		shouldFire = false;
	}

	if (shouldFire)
	{
		mInventory->makeShot();

		auto player = getPlayer();
		player->mPlayerStun += getWeaponConfig()->mStunUser;
		glm::vec2 barrelPoint = Minigine::rotateVector(getWeaponConfig()->mBarrelPoint, player->getRotationAdjusted());

		for (int i = 0; i < getWeaponConfig()->mBulletsGenerated; i++) {
			auto bullet = spawnEntity<Bullet>();
			bullet->getImage()->setSprite(getWeaponConfig()->mBullet->mSprite, getWeaponConfig()->mBullet->mSize);
			bullet->getImage()->setAlignment(0.5f, 0.5f);
			bullet->setDiameter(getWeaponConfig()->mBullet->mDiameter);
			bullet->mBulletSpeed = getWeaponConfig()->mBulletSpeed;
			bullet->mDamage = getWeaponConfig()->mDamage;
			bullet->mBulletStun = getWeaponConfig()->mStunTarget;
			bullet->mBulletHealth = getWeaponConfig()->mBulletHealth;

			// In configs degrees is specified.
			float inaccurateAngle = glm::radians(randomNextFloat(-getWeaponConfig()->mInaccuracy*0.5f, getWeaponConfig()->mInaccuracy*0.5f));

			bullet->entityMove(player->getPosition() + barrelPoint);
			bullet->entityRotate(player->getRotation() + inaccurateAngle);
		}

		SPFrameEffect fireEffect;
		fireEffect.reset(new FrameEffect(0.03f, entSize, glm::vec2(0.5f, 0), player->getRotationAdjusted(), SpriteBits::inst()->Effect_Shoot));
		fireEffect->setPosition(player->getPosition() + barrelPoint);
		mEffects.push_back(fireEffect);

		mWeaponFireDelay = randomNextFloat(getWeaponConfig()->mFireDelay.x, getWeaponConfig()->mFireDelay.y);

		auto soundIter = mSoundsWeapon.find(getWeaponConfig()->mName);
		if (soundIter == mSoundsWeapon.end()) {
			Minigine::SPSound sound = Minigine::Sound::create(getWeaponConfig()->mSound.mPath, false);
			sound->setVolume(getWeaponConfig()->mSound.mVolume);
			mSoundsWeapon[getWeaponConfig()->mName] = sound;
		}
		mSoundsWeapon[getWeaponConfig()->mName]->play();
	}
}

void
refreshZombieTexture(Zombie * zombie)
{
	zombie->getImage()->setAlignment(0.5f, 0.5f);

	MINIGINE_ASSERT(zombie->mMaxHealth > 0);
	float percentHealth = zombie->mHealth / zombie->mMaxHealth;

	Minigine::SPSprite sprite;
	if (percentHealth > 0.9f) sprite = SpriteBits::inst()->Zombie_1;
	else if (percentHealth > 0.6f) sprite = SpriteBits::inst()->Zombie_2;
	else if (percentHealth > 0.3f) sprite = SpriteBits::inst()->Zombie_3;
	else sprite = SpriteBits::inst()->Zombie_4;
	
	float size = zombie->mRadius * 2;
	zombie->getImage()->setSprite(sprite, glm::vec2(size, size));
}

void
Level::checkZombieSpawn(float fd)
{
	// return;

	if (getEntityCount(EntityType::Zombie) >= mZombieConfig->mMaxZombies) {
		return;
	}

	mZombieSpawnTimer += fd;
	if (mZombieSpawnTimer >= mZombieSpawnPeriod) {
		mZombieSpawnTimer = 0;

		auto zombie = spawnEntity<Zombie>();

		static const std::vector<float> scales = { 0.5f, 1.0f, };
		float scale = scales.at(randomNextInt(0, scales.size() - 1));

		zombie->setDiameter(36.0f + 30.0f * scale);
		zombie->mHealth = zombie->mMaxHealth = mZombieConfig->mHealth * scale;
		zombie->mMoney = static_cast<int>( randomNextInt(mZombieConfig->mMoney) * scale );
		zombie->mDamagePerSecond = mZombieConfig->mDamagePerSecond * scale;
		refreshZombieTexture(zombie);

		Minigine::Random random;
		SpawnIndex spawnIndex;
		glm::vec2 position;
		bool foundGoodSpot = false;
		for (int i = 0; i < 10; i++) {
			int indexInTable = random.nextInt(0, mSpawnIndexes.size() - 1);
			spawnIndex = mSpawnIndexes.at(indexInTable);
			position = glm::vec2(spawnIndex.x, spawnIndex.y) * tileSize + tileSize * 0.5f;
			if (raycast(position, mPlayerPosition)) {
				continue; // Player is visible, bad spawn
			}
			if (glm::distance(position, mPlayerPosition) < 500) {
				continue; // Too close to player
			}
			foundGoodSpot = true;
			break;
		}

		if (!foundGoodSpot) {
			CONSOLE_WARNING("level", "Failed to find a suitable Spawn point for zombie");
		}

		zombie->entityMove(position);
	}
}

void
Level::updateZombie(Zombie * zombie, float fd)
{
	float distanceToMove = 150.0f * fd;
	distanceToMove *= stunToVelocityScale(zombie->mZombieStun);

	stepStunValue(zombie->mZombieStun, fd);

	bool collidedWithOtherZombies = false;

	for (auto & entity : mEntities) {
		if (entity.get() == zombie) {
			continue;
		}
		if (auto otherZombie = toZombie(entity)) {
			glm::vec2 delta = zombie->getPosition() - otherZombie->getPosition();
			float distance = glm::length(delta);
			float minDistance = zombie->mRadius + otherZombie->mRadius;
			if (distance <= minDistance) {
				float insideDist = minDistance - distance;

				collidedWithOtherZombies = true;

				if (zombie->mPushFactor < otherZombie->mPushFactor) {
					continue;
				}

				if (zombie->mZombieStun > 1) {
					continue;
				}

				otherZombie->mPushFactor += 1;

				float correctionDistance = distanceToMove * 0.5f;

				glm::vec2 newPosition = zombie->getPosition() + glm::normalize(delta) * correctionDistance;
				if (!circleCollidesWithAnyTile(newPosition, zombie->mRadius)) {
					zombie->entityMove(newPosition);
					distanceToMove -= correctionDistance;
				}
			}
		}
	}

	if (entitiesCollide(zombie, getPlayer())) {
		damagePlayer(zombie->mDamagePerSecond * fd);
	}

	if (raycast(mPlayerPosition, zombie->getPosition())) {
		zombie->mLastSeenPosition = mPlayerPosition;
		zombie->mSeenPositionValid = true;
	}

	if (zombie->mSeenPositionValid) {
		glm::vec2 delta = zombie->mLastSeenPosition - zombie->getPosition();
		if (glm::length(delta) < 20) {
			zombie->mSeenPositionValid = false;
			zombie->mWalkTimer = 0.0f;
		}
		else {
			zombie->entityMove(zombie->getPosition() + glm::normalize(delta) * distanceToMove);
			float angle = atan2f(delta.y, delta.x);
			zombie->entityRotate(angle);
		}
	}
	else {
		zombie->mWalkTimer += fd;
		float rotation = zombie->getRotation();

		// When zombies are just cruising along, they move slower.
		float cruisingSpeed = distanceToMove * 0.5f;
		glm::vec2 cruiseVector = glm::vec2(cosf(rotation) * cruisingSpeed, sinf(rotation) * cruisingSpeed);
		glm::vec2 newPosition = zombie->getPosition() + cruiseVector;

		if (zombie->mWalkTimer >= 2.0f
			|| circleCollidesWithAnyTile(newPosition, zombie->mRadius)
			|| collidedWithOtherZombies)
		{
			zombie->mWalkTimer = 0;
			rotation = rotation + Minigine::PI; // Inverse direction
			rotation += randomNextFloat(0, 1) * Minigine::PI; // 180 degree random
			rotation = Minigine::normalizeAngle(rotation);
			zombie->entityRotate(rotation);
		}
		else {
			zombie->entityMove(newPosition);
		}
	}

	if (zombie->mPushFactor > 0) {
		zombie->mPushFactor -= 1;
	}
}

void
Level::checkBulletCollisions(Zombie * zombie)
{
	for (auto & entity : mEntities) {
		if (auto bullet = toBullet(entity)) {
			if (bullet->isRemoved()) {
				continue;
			}

			bool skipEntityCollision = false;
			for (auto entHandle : bullet->mSkipEntityCollisions) {
				if (entHandle == 0) {
					break;
				}
				if (entHandle == zombie->getEntityHandle()) {
					skipEntityCollision = true;
					break;
				}
			}

			if (skipEntityCollision) {
				continue;
			}

			float distance = glm::distance(zombie->getPosition(), bullet->getPosition());
			if (distance <= zombie->mRadius + bullet->mRadius) {

				bullet->mBulletHealth -= 1.0f;

				if (bullet->mBulletHealth <= 0.0f) {
					removeEntity(bullet->getEntityHandle());
				}
				else {
					bool registeredPenetration = false;
					for (auto & entHandle : bullet->mSkipEntityCollisions) {
						if (entHandle == 0) {
							entHandle = zombie->getEntityHandle();
							registeredPenetration = true;
							break;
						}
					}
					if (!registeredPenetration) {
						int size = (int)bullet->mSkipEntityCollisions.size();
						CONSOLE_ERROR("level", MINIGINE_FORMAT("Bullet could still penetrate more Zombies, but only %d penetrations are allowed.", size));
						removeEntity(bullet->getEntityHandle());
					}
				}

				zombie->mHealth -= bullet->mDamage;
				zombie->mZombieStun += bullet->mBulletStun;
				refreshZombieTexture(zombie);

				if (zombie->mHealth <= 0) {
					removeEntity(zombie->getEntityHandle());
					mTimeline->recordZombieKill();

					SPFrameEffect effect;
					effect.reset(new FrameEffect(0.1f, glm::vec2(128, 128), glm::vec2(0.5f, 0.5f), zombie->getRotationAdjusted(), SpriteBits::inst()->Effect_Zombie_Death));
					effect->setPosition(zombie->getPosition());
					mEffects.push_back(effect);

					SPMoneyEffect moneyEffect;
					moneyEffect.reset(new MoneyEffect(zombie->mMoney, 0.6f, zombie->getPosition()));
					mMoneyEffects.push_back(moneyEffect);

					mSoundZombieDeath->play();
					break;
				}
				else {
					SPFrameEffect effect;
					glm::vec2 size(zombie->mRadius * 2.0f, zombie->mRadius * 2.0f);
					effect.reset(new FrameEffect(0.03f, size, glm::vec2(0.5f, 0.5f), bullet->getRotationAdjusted(), SpriteBits::inst()->Effect_Zombie_Death));
					effect->setPosition(bullet->getPosition());
					mEffects.push_back(effect);
				}
			}
		}
	}
}

void
Level::changeZombieConfig(const ZombieConfig * config)
{
	mZombieSpawnPeriod = config->mFillTime / config->mMaxZombies;
	mZombieConfig = config;
	CONSOLE_INFO("level", MINIGINE_FORMAT("::%s - LevelTime=%f, MaxZombies=%d, FillTime=%f, SpawnPeriod=%f", config->mTag.c_str(), (float)config->mLevelTime, (int)config->mMaxZombies, (float)config->mFillTime, (float)mZombieSpawnPeriod));
}

void
Level::updateReloading(float fd)
{
	if (!isReloading()) {
		if (mInventory->magazineIsOutOfAmmo() && mInventory->hasAmmoInBackpack()) {
			beginReloading();
		}
	}

	if (isReloading()) {
		mReloadingTime += fd;
		const float max = getWeaponConfig()->mReloadTime;
		mPlayerHUD.setReloadingTime(mReloadingTime, max);
		if (mReloadingTime >= max) {
			mReloading = false;
			mInventory->reloadCurrentWeapon();
		}
	}

	if (mWasReloading != isReloading()) {
		mPlayerHUD.setReloading(isReloading());
		mWasReloading = isReloading();
	}
}

void
Level::beginReloading()
{
	MINIGINE_ASSERT_SOFT(isReloading() == false);
	mReloadingTime = 0.0f;
	mReloading = true;
}

void
Level::refreshWeapon()
{
	mWeaponFireDelay = 0.0f;
	mReloading = false;
	mReloadingTime = 0.0f;

	getPlayer()->getImage()->setSprite(PlayerPoseGetSprite(getWeaponConfig()->mPlayerPose), entSize);

	getPlayer()->mGun.setSprite(getWeaponConfig()->mSprite, getWeaponConfig()->mSize);
	getPlayer()->mGun.setAlignment(0.5f, 0.0f);
}

void
Level::damagePlayer(float damage)
{
	getPlayer()->mHealth -= damage;
	mCallback.cSetHealth(getPlayer()->mHealth, getPlayer()->mHealthMax);
	mPlayerHUD.setHealth(getPlayer()->mHealth, getPlayer()->mHealthMax);

	if (getPlayer()->mHealth <= 0) {
		mCallback.cPlayerDead();
	}

	mTimeline->recordHealth(getPlayer()->mHealth);
}

bool
Level::tileIsWalkable(int x, int y)
{
	if (x >= 0 && x < mWidth && y >= 0 && y < mHeight) {
		if (mTiles.at(x + y * mWidth) == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		MINIGINE_BREAK("Out of bounds");
		return true;
	}
}

bool
Level::circleCollidesWithAnyTile(const glm::vec2 & circlePosition, float circleRadius)
{
	for (int y = 0; y < mHeight; y++) {
		for (int x = 0; x < mWidth; x++) {
			if (!tileIsWalkable(x, y)) {
				if (circleCollidesWithTile(circlePosition, circleRadius, x, y)) {
					return true;
				}
			}
		}
	}
	return false;
}

bool
Level::circleCollidesWithTile(const glm::vec2 & circle, float circleRadius, int tileX, int tileY)
{
	// For Rotated rectangles it would be:
	// the distance between the circle-center and one vertex of your rect is smaller than the radius of your sphere 
	// OR 
	// the distance between the circle-center and one edge of your rect is smaller than the radius of your sphere ([point-line distance ]) 
	// OR 
	// the circle center is inside the rect 

	glm::vec2 tileMin(tileX * tileSize.x, tileY * tileSize.y);
	glm::vec2 tileMax = tileMin + tileSize;

	// Find the closest point to the circle within the rectangle
	float closestX = Minigine::Math::cap(tileMin.x, tileMax.x, circle.x);
	float closestY = Minigine::Math::cap(tileMin.y, tileMax.y, circle.y);

	// Calculate the distance between the circle's center and this closest point
	float distanceX = circle.x - closestX;
	float distanceY = circle.y - closestY;

	// If the distance is less than the circle's radius, an intersection occurs
	float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
	return distanceSquared < (circleRadius * circleRadius);
}

bool
Level::entitiesCollide(Entity * e1, Entity * e2)
{
	glm::vec2 delta = e1->getPosition() - e2->getPosition();
	float distance = glm::length(delta);
	float minDistance = e1->mRadius + e2->mRadius;
	return distance <= minDistance;
}

bool
Level::raycast(const glm::vec2 & starting, const glm::vec2 & ending, float * oDistance)
{
	// Implemented using this article:
	// http://lodev.org/cgtutor/raycasting.html

	// Conversion for copy pasted code
	glm::vec2 direction = glm::normalize(ending - starting);
	float rayPosX = starting.x / tileSize.x;
	float rayPosY = starting.y / tileSize.y;
	float rayDirX = direction.x;
	float rayDirY = direction.y;

	//which box of the map we're in
	int mapX = int(rayPosX);
	int mapY = int(rayPosY);

	//length of ray from current position to next x or y-side
	double sideDistX;
	double sideDistY;

	//length of ray from one x or y-side to next x or y-side
	double deltaDistX = sqrt(1 + (rayDirY * rayDirY) / (rayDirX * rayDirX));
	double deltaDistY = sqrt(1 + (rayDirX * rayDirX) / (rayDirY * rayDirY));

	//what direction to step in x or y-direction (either +1 or -1)
	int stepX;
	int stepY;

	int hit = 0; //was there a wall hit?
	int side; //was a NS or a EW wall hit?

	//calculate step and initial sideDist
	if (rayDirX < 0)
	{
		stepX = -1;
		sideDistX = (rayPosX - mapX) * deltaDistX;
	}
	else
	{
		stepX = 1;
		sideDistX = (mapX + 1.0 - rayPosX) * deltaDistX;
	}
	if (rayDirY < 0)
	{
		stepY = -1;
		sideDistY = (rayPosY - mapY) * deltaDistY;
	}
	else
	{
		stepY = 1;
		sideDistY = (mapY + 1.0 - rayPosY) * deltaDistY;
	}

	int iterations = 0;
	//perform DDA
	while (hit == 0)
	{
		iterations += 1;

		//jump to next map square, OR in x-direction, OR in y-direction
		if (sideDistX < sideDistY)
		{
			sideDistX += deltaDistX;
			mapX += stepX;
			side = 0;
		}
		else
		{
			sideDistY += deltaDistY;
			mapY += stepY;
			side = 1;
		}

		if (mapX < 0 || mapY < 0 || mapX >= mWidth || mapY >= mHeight) {
			hit = 1;
			break;
		}

		//Check if ray has hit a wall
		if (!tileIsWalkable(mapX, mapY)) hit = 1;
	} 

	if (!hit) {
		MINIGINE_BREAK("raycast is supposed to hit something..");
		return true;
	}
	else {
		float distance = 0.0f;
		if (side == 0) distance = (mapX - rayPosX + (1 - stepX) / 2) / rayDirX;
		else           distance = (mapY - rayPosY + (1 - stepY) / 2) / rayDirY;
		distance *= tileSize.x;

		// This is a bit of a hack. Its probably possible to terminate raycast
		// algorithm in the middle of execution when required distance is reached,
		// but that will add complexity. Its much easier to just check here.
		float requiredDistance = glm::distance(starting, ending);

		bool visible = false;

		if (distance >= requiredDistance) {
			visible = true;
			distance = requiredDistance;
		}

		if (oDistance) {
			*oDistance = distance;
		}

		return visible;
	}
}

void
Level::addEntity(Entity * ent)
{
	SPEntity entity = SPEntity(ent);
	mEntities.push_back(entity);
	mHandleMap[ent->getEntityHandle()] = entity;
	mEntityTypeCounts[ent->getEntityType()] += 1;
}

Player*
Level::getPlayer()
{
	Entity * ent = getEntity(mPlayerHandle);
	Player * player = toPlayer(ent);
	MINIGINE_ASSERT(player && "Player entity handle points to an Entity of wrong type!");
	return player;
}

Entity*
Level::getEntity(EntityHandle handle)
{
	auto iter = mHandleMap.find(handle);
	if (iter == mHandleMap.end()) {
		return nullptr;
	}
	return iter->second.get();
}

void
Level::removeEntity(EntityHandle handle)
{
	Entity * entity = getEntity(handle);
	MINIGINE_ASSERT_SOFT(entity);
	if (!entity) {
		return;
	}

	MINIGINE_ASSERT_SOFT(!entity->isRemoved());

	auto iter = std::find(mRemovedEntities.begin(), mRemovedEntities.end(), handle);
	MINIGINE_ASSERT_SOFT(iter == mRemovedEntities.end());
	if (iter == mRemovedEntities.end()) {
		mRemovedEntities.push_back(handle);
		entity->makeRemoved();
	}

	auto & typeCount = mEntityTypeCounts[entity->getEntityType()];
	if (typeCount >= 1) { // Should be at least 1, because entity had to be added at some point!
		typeCount -= 1;
	}
	else {
		MINIGINE_BREAK("When removing entity, type count was 0, which indicates an error in Entity Add/Remove logic");
	}
}

void
Level::destroyEntityInline(EntityHandle handle)
{
	auto mapIter = mHandleMap.find(handle);
	MINIGINE_ASSERT_SOFT(mapIter != mHandleMap.end());
	if (mapIter == mHandleMap.end()) {
		return;
	}

	SPEntity entity = mapIter->second;
	mHandleMap.erase(mapIter);

	auto iter = std::find_if(mEntities.begin(), mEntities.end(),
		[entity](const SPEntity & iter) { return entity == iter; });
	MINIGINE_ASSERT_SOFT(iter != mEntities.end());
	if (iter != mEntities.end()) {
		mEntities.erase(iter);
	}
}

int
Level::getEntityCount(EntityType type) const
{
	auto iter = mEntityTypeCounts.find(type);
	if (iter == mEntityTypeCounts.end()) {
		return 0;
	}
	int entityTypeCount = iter->second;
	MINIGINE_ASSERT_SOFT(entityTypeCount >= 0 && "Negative Entity Type Count indicates error in Add/Remove entity logic");
	return entityTypeCount;
}
