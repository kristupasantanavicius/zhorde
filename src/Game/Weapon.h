#pragma once

#include <Game/SpriteBits.h>
#include <Game/PlayerPose.h>
#include <Game/WeaponName.h>
#include <minigine/utils/Math.h>
#include <minigine/Singleton.h>

enum class BulletName
{
	None,
	Round,
	Shrapnel,
};

class BulletConfig
{
public:
	BulletName mName = BulletName::None;
	glm::vec2 mSize;
	float mDiameter = 0.0f;
	Minigine::SPSprite mSprite;

private:
	BulletConfig() { }
	friend class BulletConfigs;

};

struct WeaponSound
{
	WeaponSound() { }
	WeaponSound(const char * path, float volume) : mPath(path), mVolume(volume) { }
	const char * mPath = nullptr;
	float mVolume = 0.0f;
};

class WeaponConfig
{
public:
	WeaponName mName = WeaponName::None;
	glm::vec2 mSize;
	glm::vec2 mOffset;
	glm::vec2 mBarrelPoint;
	Minigine::SPSprite mSprite;
	Minigine::SPSprite mIcon;

	int mCost = 0;
	int mAmmoCost = 0;
	int mAmmoMax = 0;
	bool mAutoFire = false;
	float mBulletSpeed = 0.0f;
	glm::vec2 mFireDelay;
	float mDamage = 0.0f;
	float mBulletHealth = 0;
	float mMovementSpeedPentalty = 0.0f;
	float mStunUser = 0;
	float mStunTarget = 0;
	float mReloadTime = 0.0f;
	float mInaccuracy = 0.0f;
	int mBulletsGenerated = 0;

	const BulletConfig * mBullet = nullptr;

	PlayerPose mPlayerPose = PlayerPose::None;
	WeaponSound mSound;

private:
	WeaponConfig() { }
	friend class WeaponConfigs;

};

class BulletConfigs : public Singleton<BulletConfigs>
{
public:
	const BulletConfig Round = []() {
		BulletConfig bullet;
		bullet.mName = BulletName::Round;
		bullet.mSize = glm::vec2(12, 12);
		bullet.mDiameter = 12;
		bullet.mSprite = SpriteBits::inst()->Bullet_Round;
		return bullet;
	}();

	const BulletConfig Shrapnel = []() {
		BulletConfig bullet;
		bullet.mName = BulletName::Shrapnel;
		bullet.mSize = glm::vec2(12, 12);
		bullet.mDiameter = 12;
		bullet.mSprite = SpriteBits::inst()->Bullet_Shrapnel;
		return bullet;
	}();

};

class WeaponConfigs : public Singleton<WeaponConfigs>
{
public:
	const WeaponConfig Pistol = []() {
		WeaponConfig weapon;
		weapon.mName = WeaponName::Pistol;
		weapon.mSize = glm::vec2(48, 48);
		weapon.mOffset = glm::vec2(12, 0);
		weapon.mBarrelPoint = glm::vec2(11, 34);
		weapon.mSprite = SpriteBits::inst()->Gun_Pistol;
		weapon.mIcon = SpriteBits::inst()->Icon_Pistol;
		weapon.mCost = 0;
		weapon.mAmmoCost = 40;
		weapon.mAmmoMax = 13;
		weapon.mAutoFire = false;
		weapon.mBulletSpeed = 1200.0f;
		weapon.mFireDelay = glm::vec2(0.17f, 0.17f);
		weapon.mInaccuracy = 0.0f;
		weapon.mBulletsGenerated = 1;
		weapon.mDamage = 1.0f;
		weapon.mBulletHealth = 1.0f;
		weapon.mStunTarget = 1.0f;
		weapon.mReloadTime = 1.0f;
		weapon.mBullet = &BulletConfigs::inst()->Round;
		weapon.mPlayerPose = PlayerPose::Hand;
		weapon.mSound = WeaponSound("resources/sounds/pistolsound.wav", 0.25f);
		return weapon;
	}();

	const WeaponConfig Tec9 = []() {
		WeaponConfig weapon;
		weapon.mName = WeaponName::Tec9;
		weapon.mSize = glm::vec2(48, 48);
		weapon.mOffset = glm::vec2(13, 0);
		weapon.mBarrelPoint = glm::vec2(11, 42);
		weapon.mSprite = SpriteBits::inst()->Gun_Tec9;
		weapon.mIcon = SpriteBits::inst()->Icon_Tec9;
		weapon.mCost = 500;
		weapon.mAmmoCost = 100;
		weapon.mAmmoMax = 28;
		weapon.mAutoFire = true;
		weapon.mBulletSpeed = 1200.0f;
		weapon.mFireDelay = glm::vec2(0.08f, 0.08f);
		weapon.mInaccuracy = 15.0f;
		weapon.mBulletsGenerated = 1;
		weapon.mDamage = 1.0f;
		weapon.mBulletHealth = 1;
		weapon.mStunUser = 1.0f;
		weapon.mStunTarget = 1.0f;
		weapon.mReloadTime = 2.0f;
		weapon.mBullet = &BulletConfigs::inst()->Round;
		weapon.mPlayerPose = PlayerPose::Hand;
		weapon.mSound = WeaponSound("resources/sounds/pistolsound.wav", 0.25f);
		return weapon;
	}();

	const WeaponConfig Shotgun = []() {
		WeaponConfig weapon;
		weapon.mName = WeaponName::Shotgun;
		weapon.mSize = glm::vec2(48, 48);
		weapon.mOffset = glm::vec2(15, -3);
		weapon.mBarrelPoint = glm::vec2(16, 26);
		weapon.mSprite = SpriteBits::inst()->Gun_Shotgun;
		weapon.mIcon = SpriteBits::inst()->Icon_Shotgun;
		weapon.mCost = 1000;
		weapon.mAmmoCost = 200;
		weapon.mAmmoMax = 2;
		weapon.mAutoFire = false;
		weapon.mBulletSpeed = 1200.0f;
		weapon.mFireDelay = glm::vec2(0.5f, 0.5f);
		weapon.mInaccuracy = 30.0f;
		weapon.mBulletsGenerated = 5;
		weapon.mDamage = 1.0f;
		weapon.mBulletHealth = 3.0f;
		weapon.mStunUser = 6.0f;
		weapon.mStunTarget = 2.0f;
		weapon.mReloadTime = 4.0f;
		weapon.mBullet = &BulletConfigs::inst()->Shrapnel;
		weapon.mPlayerPose = PlayerPose::Heavy;
		weapon.mSound = WeaponSound("resources/sounds/shotgunsound.wav", 0.25f);
		return weapon;
	}();

};
