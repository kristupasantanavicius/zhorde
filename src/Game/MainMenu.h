#pragma once

#include <Game/UICallback.h>
#include <EngineEXT/DialogComponent.h>

class MainMenu : public DialogComponent
{
public:
	MainMenu(UICallback callback);

private:
	UICallback mCallback;

};
