
#include <Game/Console.h>
#include <minigine/Format.h>

static std::vector<ConsoleMessage> ConsoleEntries;

void CONSOLE_ECHO_func(const std::string & filter, const std::string & msg, ConsoleLogLevel level)
{
#ifdef DEBUG
	std::string combined = MINIGINE_FORMAT("[%s]: %s", filter.c_str(), msg.c_str());
	ConsoleMessage message;
	message.text = combined;
	message.level = level;
	ConsoleEntries.push_back(message);
#endif
}

void
ConsoleFlushMessages(std::vector<ConsoleMessage> & out)
{
	out = std::move(ConsoleEntries);
}
