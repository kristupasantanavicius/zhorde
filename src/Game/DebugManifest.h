#pragma once

#include <minigine/Singleton.h>

class DebugManifest : public Singleton<DebugManifest>
{
public:
	void toggleDisplayCollisionMeshes() { mDisplayCollisionMeshes = !mDisplayCollisionMeshes; }
	bool displayCollisionMeshes() { return mDisplayCollisionMeshes; }

private:
	bool mDisplayCollisionMeshes = false;

};
