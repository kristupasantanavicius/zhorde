#pragma once

enum class WeaponName
{
	None,
	Pistol,
	Tec9,
	Shotgun,
};

inline const char*
WeaponNameToString(WeaponName name)
{
	switch (name) {
		case WeaponName::None: return "weapon_name_none";
		case WeaponName::Pistol: return "Pistol";
		case WeaponName::Tec9: return "Tec9";
		case WeaponName::Shotgun: return "Shotgun";
		default: MINIGINE_BREAK("not implemented enum value"); return "weapon_no_name";
	}
}
