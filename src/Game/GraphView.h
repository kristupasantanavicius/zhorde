#pragma once

#include <Game/Timeline.h>
#include <EngineEXT/DialogComponent.h>
#include <minigine/ComplexObject.h>
#include <minigine/ImageVO.h>
#include <minigine/StringVO.h>
#include <minigine/Sound.h>
#include <minigine/ui/ComplexButton.h>

class GraphView : public DialogComponent
{
public:
	GraphView(SPTimeline timeline)
	: mTimeline(timeline)
	{ init(); }

private:
	void init();

private:
	SPTimeline mTimeline;

};
