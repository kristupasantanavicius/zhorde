#pragma once

struct InventoryListener {
	virtual ~InventoryListener() { }
	virtual void inventoryAmmoChanged(int magazine, int backpack) = 0;
	virtual void inventoryMoneyChanged(int coins) = 0;
};
