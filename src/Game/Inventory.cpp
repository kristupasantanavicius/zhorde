#include <Game/Inventory.h>

Inventory::Inventory()
{
	addDefaultWeapon();
}

void
Inventory::clearInventory()
{
	mMoney = 0;
	notifyMoney();

	mWeapons.clear();
	mSelected = -1;
	addDefaultWeapon();
}

void
Inventory::createWeapon(const WeaponConfig * config)
{
	SPWeapon weapon = SPWeapon(new Weapon());
	weapon->mConfig = config;
	weapon->mAmmoInBackpack = weapon->mConfig->mAmmoMax * 2;
	mWeapons.push_back(weapon);
	reloadWeapon(weapon.get());
}

void
Inventory::addDefaultWeapon()
{
	MINIGINE_ASSERT_SOFT(mWeapons.empty());
	mSelected = 0;
	createWeapon(&WeaponConfigs::inst()->Pistol);
}

void
Inventory::makeShot()
{
	if (magazineIsOutOfAmmo()) {
		MINIGINE_BREAK("Can't fire a shot, because magazine is empty!");
		return;
	}
	getWeapon()->mAmmoInMagazine -= 1;
	notifyAmmo();
}

void
Inventory::reloadCurrentWeapon()
{
	reloadWeapon(getWeapon());
}

void
Inventory::reloadWeapon(Weapon * weapon)
{
	int ammoNeeded = weapon->mConfig->mAmmoMax - weapon->mAmmoInMagazine;
	if (weapon->mAmmoInBackpack >= ammoNeeded) {
		weapon->mAmmoInBackpack -= ammoNeeded;
		weapon->mAmmoInMagazine += ammoNeeded;
	}
	else {
		weapon->mAmmoInMagazine += weapon->mAmmoInBackpack;
		weapon->mAmmoInBackpack = 0;
	}
	notifyAmmo();
}

bool
Inventory::hasAmmoInBackpack()
{
	return getWeapon()->mAmmoInBackpack >= 1;
}

bool
Inventory::magazineIsOutOfAmmo()
{
	return getWeapon()->mAmmoInMagazine == 0;
}

bool
Inventory::magazineIsFull()
{
	return getWeapon()->mAmmoInMagazine >= getWeapon()->mConfig->mAmmoMax;
}

void
Inventory::selectNextWeapon()
{
	if (mSelected >= 1) {
		mSelected--;
	}
	notifyAmmo();
}

void
Inventory::selectPreviousWeapon()
{
	if (mSelected < static_cast<int>(mWeapons.size()) - 1) {
		mSelected++;
	}
	notifyAmmo();
}

Weapon*
Inventory::editWeaponByConfig(const WeaponConfig * config)
{
	for (auto & weapon : mWeapons) {
		if (weapon->mConfig->mName == config->mName) {
			return weapon.get();
		}
	}
	return nullptr;
}

Weapon*
Inventory::getWeapon()
{
	MINIGINE_ASSERT(mSelected >= 0 && mSelected <= static_cast<int>(mWeapons.size()));
	return mWeapons.at(mSelected).get();
}

bool
Inventory::hasEnoughMoney(int req)
{
	return getMoney() >= req;
}

int
Inventory::getMoney()
{
	return mMoney;
}

void
Inventory::purchaseWeapon(const WeaponConfig * config)
{
	if (!hasEnoughMoney(config->mCost)) {
		MINIGINE_BREAK("Can't purchase weapon, not enough money");
		return;
	}
	for (auto & weapon : mWeapons) {
		if (weapon->mConfig->mName == config->mName) {
			MINIGINE_BREAK("Trying to purchase a weapon that is already owned");
			return;
		}
	}
	createWeapon(config);
	mSelected = static_cast<int>(mWeapons.size() - 1);
	notifyAmmo();

	mMoney -= config->mCost;
	notifyMoney();
}

void
Inventory::purchaseAmmo(const WeaponConfig * config)
{
	if (!hasEnoughMoney(config->mAmmoCost)) {
		MINIGINE_BREAK("Can't purchase Ammo, not enough money");
		return;
	}
	Weapon * weapon = editWeaponByConfig(config);
	if (!weapon) {
		MINIGINE_BREAK("Trying to purchase Ammo for weapon that isn't in Inventory");
		return;
	}

	weapon->mAmmoInBackpack += config->mAmmoMax;
	if (weapon == getWeapon()) {
		notifyAmmo();
	}

	mMoney -= config->mAmmoCost;
	notifyMoney();
}

void
Inventory::addMoney(int money)
{
	mMoney += money;
	notifyMoney();
}

void
Inventory::addInventoryListener(InventoryListener & listener)
{
	auto iter = std::find(mListeners.begin(), mListeners.end(), &listener);
	MINIGINE_ASSERT_SOFT(iter == mListeners.end());
	if (iter == mListeners.end()) {
		mListeners.push_back(&listener);
	}

	listener.inventoryAmmoChanged(getWeapon()->mAmmoInMagazine, getWeapon()->mAmmoInBackpack);
	listener.inventoryMoneyChanged(mMoney);
}

void
Inventory::removeInventoryListener(InventoryListener & listener)
{
	auto iter = std::find(mListeners.begin(), mListeners.end(), &listener);
	MINIGINE_ASSERT(iter != mListeners.end());
	mListeners.erase(iter);
}

void
Inventory::notifyAmmo()
{
	for (auto & listener : mListeners) {
		listener->inventoryAmmoChanged(getWeapon()->mAmmoInMagazine, getWeapon()->mAmmoInBackpack);
	}
}

void
Inventory::notifyMoney()
{
	for (auto & listener : mListeners) {
		listener->inventoryMoneyChanged(mMoney);
	}
}
