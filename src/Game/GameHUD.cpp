#include <Game/GameHUD.h>
#include <EngineEXT/UIHelpers/GUIHelper.h>
#include <minigine/utils/String.h>
#include <minigine/Format.h>

const glm::vec2 healthBarSize(200, 50);
const glm::vec2 healthBarInset(5, 5);

void
GameHUD::init()
{
	auto screenSize = Minigine::getScreenSize();

	mHealthBackground = GUI_colorpad(glm::vec4(0.3f, 0.3f, 0.3f, 1), healthBarSize);
	mHealthBackground->setPosition(screenSize.x - healthBarSize.x, screenSize.y - healthBarSize.y);
	addToScreen(mHealthBackground);

	mHealthBar = GUI_colorpad(glm::vec4(1, 0, 0, 1), glm::vec2(200, 200));
	mHealthBar->setPosition(screenSize.x - healthBarSize.x + healthBarInset.x, screenSize.y - healthBarSize.y + healthBarInset.y);
	mHealthBar->setHeight(healthBarSize.y - healthBarInset.y * 2);
	addToScreen(mHealthBar);

	auto coinIcon = GUI_image(SpriteBits::inst()->UI_Coin, glm::vec2(32, 32));
	coinIcon->setAlignment(1.0f, 0.5f);
	coinIcon->setPosition(screenSize.x - healthBarSize.x - 120, screenSize.y - 25);
	addToScreen(coinIcon);

	GUI_StringParams params;
	params.mFontSize = 30;
	params.mString = "coins";
	mCoins = GUI_string(params);
	mCoins->setAlignment(0.0f, 0.5f);
	mCoins->setPosition(coinIcon->getPosition() + glm::vec2(10, 0));
	addToScreen(mCoins);

	params.mString = "ammo";
	mAmmo = GUI_string(params);
	mAmmo->setPosition(mCoins->getPosition() + glm::vec2(0, -40));
	addToScreen(mAmmo);

	params.mString = "level_time";
	mLevelTime = GUI_string(params);
	mLevelTime->setPosition(mCoins->getPosition() + glm::vec2(0, -70));
	addToScreen(mLevelTime);

	mInventory->addInventoryListener(*this);
}

GameHUD::~GameHUD()
{
	mInventory->removeInventoryListener(*this);
}

void
GameHUD::setPlayerHealth(float health, float maxHealth)
{
	float healthNormalized = health / maxHealth;
	mHealthBar->setWidth((healthBarSize.x - healthBarInset.x * 2) * healthNormalized);
}

void
GameHUD::setLevelTime(float time)
{
	int minutes = (int)time / 60;
	int seconds = (int)time % 60;
	mLevelTime->setString(MINIGINE_FORMAT("%dm %ds (%.1f)", minutes, seconds, time));
}

void
GameHUD::inventoryAmmoChanged(int magazine, int backpack)
{
	mAmmo->setString(Minigine::to_string(magazine) + "/" + Minigine::to_string(backpack));
}

void
GameHUD::inventoryMoneyChanged(int coins)
{
	mCoins->setString(Minigine::to_string(coins));
}
