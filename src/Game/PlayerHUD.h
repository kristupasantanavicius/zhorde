#pragma once

#include <EngineEXT/EllipseVO.h>
#include <minigine/ComplexObject.h>
#include <minigine/VisibleObject.h>
#include <minigine/StringVO.h>
#include <minigine/ColorPadVO.h>

class PlayerHUD
{
public:
	PlayerHUD() { init(); }

	void setPosition(const glm::vec2 & position);
	void setHealth(float health, float maxHealth);
	void setStun(float stun, float max);
	void setReloading(bool reloading);
	void setReloadingTime(float time, float max);

	void update(float fd);
	void draw(const Minigine::DrawInfo & drawInfo);

private:
	void init();

private:
	Minigine::SPComplexObject mComplex;
	Minigine::Ext::SPEllipseVO mEllipse;
	Minigine::Ext::SPEllipseVO mStun;
	Minigine::SPStringVO mStringReloading;
	Minigine::SPColorPadVO mReloadingBar;
	Minigine::SPComplexObject mReloadingComplex;
	float mAnimation = 0.0f;

};
