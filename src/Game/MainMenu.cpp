#include <Game/MainMenu.h>
#include <EngineEXT/UIHelpers/GUIHelper.h>
#include <minigine/ui/ComplexButton.h>

MainMenu::MainMenu(UICallback callback)
: mCallback(callback)
{
	auto screenSize = Minigine::getScreenSize();

	auto createButton = [this, screenSize](int index, std::string text, std::function<void()> callback) {
		auto complex = GUI_panel(200.0f, 50.0f);
		complex->setAlignment(0.5f, 0.5f);
		complex->setPosition(complex->getSize() * 0.5f);

		auto background = GUI_colorpad(glm::vec4(0.4f, 0.4f, 0.4f, 1.0f), complex->getSize());
		complex->add(background);

		GUI_StringParams params;
		params.mString = text;
		params.mFontSize = 30;
		auto string = GUI_string(params);
		string->setAlignment(0.5f, 0.5f);
		string->setPosition(complex->getSize() * 0.5f);
		complex->add(string);

		Minigine::SPComplexButton button;
		button.reset(new Minigine::ComplexButton(complex));
		button->setPosition(glm::vec2(200.0f, screenSize.y - 200.0f - index * (complex->getHeight() + 50.0f) ));
		addToScreen(button);

		button->setCallback([callback](Minigine::VisibleObject2d*) { callback(); });
	};

	createButton(0, "Start", [this]() { mCallback.cStartGame(); });
	createButton(1, "Exit", [this]() { mCallback.cExitGame(); });
}
