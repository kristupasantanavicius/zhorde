#pragma once

#include <minigine/Sprite.h>
#include <minigine/ImageVO.h>
#include <vector>

class FrameEffect
{
public:
	FrameEffect(float frameTime, glm::vec2 size, glm::vec2 alignment, float rotation, std::vector<Minigine::SPSprite> sprites)
	: mSprites(sprites)
	, mFrameTime(frameTime)
	, mSize(size)
	{
		mImage.setRotation(glm::degrees(rotation));
		mImage.setAlignment(alignment);
		mImage.setSprite(mSprites.at(mCurrentFrame), mSize);
	}

	void setPosition(const glm::vec2 & position) {
		mImage.setPosition(position);
	}

	bool update(float fd) {
		mTime += fd;
		if (mTime >= mFrameTime) {
			mTime -= mFrameTime;
			mCurrentFrame++;
			if (mCurrentFrame >= static_cast<int>(mSprites.size())) {
				return true;
			}
			else {
				mImage.setSprite(mSprites.at(mCurrentFrame), mSize);
			}
		}
		return false;
	}

	void draw(const Minigine::DrawInfo & drawInfo) {
		mImage.draw(drawInfo);
	}

private:
	std::vector<Minigine::SPSprite> mSprites;
	Minigine::ImageVO mImage;
	const float mFrameTime = 0.0f;
	float mTime = 0.0f;
	int mCurrentFrame = 0;
	glm::vec2 mSize;

};

typedef std::shared_ptr<FrameEffect> SPFrameEffect;
