#pragma once

#include <EngineEXT/EllipseVO.h>

class CircleRender
{
public:
	static void
	draw(const Minigine::DrawInfo & drawInfo, const glm::vec2 & position, float radius, const glm::vec4 & color)
	{
		static Minigine::Ext::SPEllipseVO gCircle = Minigine::Ext::SPEllipseVO();
		if (!gCircle) {
			gCircle.reset(new Minigine::Ext::EllipseVO(glm::vec2(100, 100), 15, false, 2, glm::vec4(1, 1, 1, 1), glm::vec4(1, 1, 1, 1)));
		}

		float scale = (radius * 2.0f) / gCircle->getWidth();
		gCircle->setScale(scale, scale);

		gCircle->setPosition(position);
		gCircle->setColor(color);
		gCircle->draw(drawInfo);
	}

};
