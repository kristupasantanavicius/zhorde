#pragma once

#include <Game/Entity.h>
#include <Game/WeaponName.h>
#include <Game/LevelCallback.h>
#include <Game/PlayerHUD.h>
#include <Game/Inventory.h>
#include <Game/Timeline.h>
#include <EngineEXT/Components.h>
#include <minigine/Camera.h>
#include <minigine/VisibleObject.h>
#include <minigine/Sound.h>
#include <unordered_map>

class WeaponConfig;
class FrameEffect;
class Player;
class Zombie;
class MoneyEffect;
class ZombieConfig;

class Level : public ComponentBase
{
public:
	Level(SPInventory inventory, SPTimeline timeline, LevelCallback callback);

	void update(float fd) override;
	void render(const Minigine::DrawInfo & drawInfo) override;

	Minigine::VisibleObject2d::TouchHandleResult
	pointerEvent(const Minigine::PointerEvent & event) override;
	
	void keyPressed(int key);

	void setLevelPaused(bool paused);

private:
	void checkFiring(float fd);
	void checkZombieSpawn(float fd);
	void updateZombie(Zombie * zombie, float fd);
	void checkBulletCollisions(Zombie * zombie);
	void changeZombieConfig(const ZombieConfig * config);

	const Weapon * getCurrentWeapon() { return mInventory->getCurrentWeapon(); }
	const WeaponConfig * getWeaponConfig() { return mInventory->getCurrentWeapon()->mConfig; }
	void refreshWeapon();

	void updateReloading(float fd);
	void beginReloading();
	bool isReloading() { return mReloading; }

	void damagePlayer(float damage);

	bool tileIsWalkable(int x, int y);

	bool circleCollidesWithAnyTile(const glm::vec2 & circlePosition, float circleRadius);
	bool circleCollidesWithTile(const glm::vec2 & circlePosition, float circleRadius, int tileX, int tileY);
	bool entitiesCollide(Entity * e1, Entity * e2);

	bool raycast(const glm::vec2 & starting, const glm::vec2 & ending, float * distance = nullptr);

	template <typename T>
	T * spawnEntity() {
		static_assert(std::is_base_of<Entity, T>::value, "spawnEntity() only accepts types derived from Entity");
		mEntityHandle += 1;
		T * ent = new T(mEntityHandle);
		addEntity(ent);
		return ent;
	}

	void addEntity(Entity * entity);

	Player * getPlayer();
	Entity * getEntity(EntityHandle handle);
	void removeEntity(EntityHandle handle);
	void destroyEntityInline(EntityHandle handle);

	int getEntityCount(EntityType type) const;

private:
	EntityHandle mEntityHandle = 0;
	std::vector<SPEntity> mEntities;
	std::unordered_map<EntityHandle, SPEntity> mHandleMap;
	std::vector<EntityHandle> mRemovedEntities;
	std::unordered_map<EntityType, int> mEntityTypeCounts;
	int mPlayerHandle = 0;
	glm::vec2 mPointerPosition;
	bool mFiring = false;
	bool mWasFiring = false;
	float mZombieSpawnTimer = 0;

	std::vector<std::shared_ptr<FrameEffect>> mEffects;

	glm::vec2 mPlayerPosition;

	Minigine::SPSound mSoundZombieDeath;
	std::map<WeaponName, Minigine::SPSound> mSoundsWeapon;

	Camera mCamera;

	int mWidth = 0, mHeight = 0;
	std::vector<char> mTiles;

	typedef uint16_t SpawnIndex_t;
	struct SpawnIndex { SpawnIndex_t x, y; };
	std::vector<SpawnIndex> mSpawnIndexes;

	SPInventory mInventory;
	float mWeaponFireDelay = 0.0f;
	bool mReloading = false, mWasReloading = false;
	float mReloadingTime = 0.0f;

	glm::vec2 mBoundsMin, mBoundsMax;

	LevelCallback mCallback;

	PlayerHUD mPlayerHUD;

	bool mPaused = false;

	std::vector<std::shared_ptr<MoneyEffect>> mMoneyEffects;

	float mLevelTime = 0;
	float mZombieSpawnPeriod = 0;
	const ZombieConfig * mZombieConfig = nullptr;

	Minigine::SPColorPadVO mLaser;

	SPTimeline mTimeline;

};
