#include <Game/Shop.h>
#include <EngineEXT/UIHelpers/GUIHelper.h>
#include <minigine/ui/ComplexButton.h>
#include <minigine/GUIList.h>
#include <minigine/utils/String.h>

void
Shop::init()
{
	mSoundNoMoney = Minigine::Sound::create("resources/sounds/fart.wav", false);

	auto screenSize = Minigine::getScreenSize();

	auto root = GUI_panel(768, 0);
	root->setActive(true);
	root->setAlignment(0.5f, 0.0f);
	root->setPosition(screenSize.x * 0.5f, 0.0f);
	addToScreen(root);

	auto shopComplex = GUI_panel(768, 128); {
		auto colorpad = GUI_colorpad(glm::vec4(0, 1, 0, 1), shopComplex->getSize());
		shopComplex->add(colorpad);
		shopComplex->setPosition(0, 0);
		shopComplex->setActive(true);
		root->add(shopComplex);

		GUI_StringParams params;
		params.mString = "S  H  O  P";
		params.mFontSize = 100;
		auto caption = GUI_string(params);
		caption->setAlignment(0.5f, 0.5f);
		caption->setPosition(shopComplex->getSize() * 0.5f);
		caption->setColor(glm::vec4(1.0f, 1.0f, 1.0f, 0.15f));
		caption->setOrder(1);
		shopComplex->add(caption);

		mComplexItemsShop = GUI_panel(shopComplex->getSize());
		mComplexItemsShop->setOrder(2);
		shopComplex->add(mComplexItemsShop);

		for (int i = 0; i < 6; i++) {
			SPWeaponSlot slot = createWeaponSlot(i, false);
			mSlotsShop.push_back(slot);
			mSlotsAll.push_back(slot);
			shopComplex->add(slot->mComplex);
		}
	}

	auto inventoryComplex = GUI_panel(768, 128); {
		auto colorpad = GUI_colorpad(glm::vec4(1, 0, 0, 1), inventoryComplex->getSize());
		inventoryComplex->add(colorpad);
		inventoryComplex->setPosition(0, 128);
		inventoryComplex->setActive(true);
		root->add(inventoryComplex);

		GUI_StringParams params;
		params.mString = "I N V E N T O R Y";
		params.mFontSize = 100;
		auto caption = GUI_string(params);
		caption->setAlignment(0.5f, 0.5f);
		caption->setPosition(inventoryComplex->getSize() * 0.5f);
		caption->setColor(glm::vec4(1.0f, 1.0f, 1.0f, 0.15f));
		caption->setOrder(1);
		inventoryComplex->add(caption);

		mComplexItemsInventory = GUI_panel(inventoryComplex->getSize());
		mComplexItemsInventory->setOrder(2);
		inventoryComplex->add(mComplexItemsInventory);

		for (int i = 0; i < 6; i++) {
			SPWeaponSlot slot = createWeaponSlot(i, true);
			mSlotsInventory.push_back(slot);
			mSlotsAll.push_back(slot);
			inventoryComplex->add(slot->mComplex);
		}
	}

	auto weaponComplex = GUI_panel(350, 180); {
		auto colorpad = GUI_colorpad(glm::vec4(0.243, 0.454, 0.627, 1), weaponComplex->getSize());
		weaponComplex->add(colorpad);
		weaponComplex->setPosition(shopComplex->getWidth() / 2, 256);
		weaponComplex->setAlignment(0.5f, 0.0f);
		weaponComplex->setActive(true);
		root->add(weaponComplex);

		mInfo.mIcon.reset(new Minigine::ImageVO(SpriteBits::inst()->WTF, glm::vec2(64, 64)));
		mInfo.mIcon->setPosition(20.0f, weaponComplex->getHeight() - mInfo.mIcon->getHeight() - 20.0f);
		weaponComplex->add(mInfo.mIcon);

		GUI_StringParams params;
		params.mString = "Pistol";
		params.mFontSize = 30;
		mInfo.mName = GUI_string(params);
		mInfo.mName->setPosition(mInfo.mIcon->getPosition() + glm::vec2(mInfo.mIcon->getWidth() + 20.0f, mInfo.mIcon->getWidth() * 0.5f));
		mInfo.mName->setAlignment(0.0f, 0.5f);
		mInfo.mName->setColor(glm::vec4(1, 1, 1, 0.5f));
		weaponComplex->add(mInfo.mName);

		{
			auto buttonPurchase = createPurchaseButton(mInfo.mButtonBuyWeapon);
			buttonPurchase->setPosition(weaponComplex->getWidth() * 0.5f, weaponComplex->getHeight() - 140);
			buttonPurchase->setCallback([this](Minigine::VisibleObject2d*) { onButtonPurchaseWeapon(); });
			weaponComplex->add(buttonPurchase);
		}

			mInfo.mComplexAmmo = GUI_panel(0, 0); {
			mInfo.mComplexAmmo->setActive(true);
			mInfo.mComplexAmmo->setPosition(weaponComplex->getWidth() / 2 - 30, weaponComplex->getHeight() - 140);
			weaponComplex->add(mInfo.mComplexAmmo);

			{
				auto buttonPurchase = createPurchaseButton(mInfo.mButtonBuyAmmo);
				buttonPurchase->setAlignment(0.0f, 0.5f);
				buttonPurchase->setPosition(5, 0);
				buttonPurchase->setCallback([this](Minigine::VisibleObject2d*) { onButtonPurchaseAmmo(); });
				mInfo.mComplexAmmo->add(buttonPurchase);
				updatePriceButton(300, mInfo.mButtonBuyAmmo);
			}

			GUI_StringParams params;
			params.mString = "+12";
			params.mFontSize = 30;
			mInfo.mStringAmmoGain = GUI_string(params);
			mInfo.mStringAmmoGain->setAlignment(1.0f, 0.5f);
			mInfo.mStringAmmoGain->setPosition(-5, 0);
			mInfo.mStringAmmoGain->setColor(glm::vec4(1, 1, 1, 0.5f));
			mInfo.mComplexAmmo->add(mInfo.mStringAmmoGain);

			params.mString = "0/12";
			mInfo.mStringAmmoAmount = GUI_string(params);
			mInfo.mStringAmmoAmount->setAlignment(1.0f, 0.0f);
			mInfo.mStringAmmoAmount->setPosition(-5, 30);
			mInfo.mComplexAmmo->add(mInfo.mStringAmmoAmount);

			params.mString = ":AMMO";
			auto ammoCaption = GUI_string(params);
			ammoCaption->setAlignment(0.0f, 0.0f);
			ammoCaption->setPosition(5, 30);
			mInfo.mComplexAmmo->add(ammoCaption);
		}
	}

	reloadItems();

	// Player always has a pistol, so select it.
	selectSlot(mSlotsInventory.at(0));
}

void
Shop::onShow()
{
	MINIGINE_ASSERT_SOFT(mSelectedSlot);
	if (mSelectedSlot) {
		fillWeaponInfo(mSelectedSlot);
	}
}

void
Shop::fillWeaponInfo(const SPWeaponSlot & slot)
{
	MINIGINE_ASSERT(slot->mItem);
	MINIGINE_ASSERT(slot->mItem->mConfig);
	const WeaponConfig * config = slot->mItem->mConfig;
	mInfo.mIcon->setSprite(config->mIcon, mInfo.mIcon->getSize());
	mInfo.mName->setString(WeaponNameToString(config->mName));

	mInfo.mButtonBuyWeapon.mButton->setVisible(false);
	mInfo.mComplexAmmo->setVisible(false);

	const Weapon * weapon = mInventory->getWeaponByConfig(config);
	if (weapon) {
		mInfo.mComplexAmmo->setVisible(true);
		int totalAmmo = weapon->mAmmoInMagazine + weapon->mAmmoInBackpack;
		mInfo.mStringAmmoAmount->setString(Minigine::to_string(totalAmmo));
		mInfo.mStringAmmoGain->setString("+" + Minigine::to_string(config->mAmmoMax));
		updatePriceButton(config->mAmmoCost, mInfo.mButtonBuyAmmo);
	}
	else {
		mInfo.mButtonBuyWeapon.mButton->setVisible(true);
		updatePriceButton(config->mCost, mInfo.mButtonBuyWeapon);
	}
}

Shop::SPWeaponSlot
Shop::createWeaponSlot(int index, bool colorOther)
{
	SPWeaponSlot slot; slot.reset(new WeaponSlot());
	slot->mComplex = GUI_panel(128, 128);
	slot->mComplex->setPosition(128.0f * index, 0.0f);
	slot->mComplex->setActive(true);

	glm::vec4 color(0.419, 0.729, 0.419, 1.0f);
	if (index % 2 == 1) {
		color *= 0.7f;
		color.a = 1.0f;
	}

	if (colorOther) {
		color = glm::vec4(0.858, 0.647, 0.403, 1.0f);
		if (index % 2 == 0) {
			color *= 0.7f;
			color.a = 1.0f;
		}
	}

	auto colorpad = GUI_colorpad(color, slot->mComplex->getSize());
	colorpad->setActive(true);
	colorpad->setCallback([this, slot](Minigine::VisibleObject2d*) { onSlotClick(slot); } );
	slot->mComplex->add(colorpad);

	slot->mIconSelected.reset(new Minigine::ImageVO(SpriteBits::inst()->UI_SlotSelected, slot->mComplex->getSize() * 0.9f));
	slot->mIconSelected->setVisible(false);
	slot->mIconSelected->setAlignment(0.5f, 0.5f);
	slot->mIconSelected->setPosition(slot->mComplex->getSize() * 0.5f);
	slot->mComplex->add(slot->mIconSelected);

	return slot;
}

void
Shop::moveItemToSlot(const SPShopItem & item, const SPWeaponSlot & slot)
{
	glm::vec2 position = slot->mComplex->getPosition() + slot->mComplex->getSize() * 0.5f;
	item->mComplex->setPosition(position - item->mComplex->getSize() * 0.5f);
	slot->mItem = item;
}

void
Shop::onSlotClick(const SPWeaponSlot & slot)
{
	if (slot->mItem) {
		selectSlot(slot);
	}
}

void
Shop::selectSlot(const SPWeaponSlot & slot)
{
	if (mSelectedSlot) {
		deselectSlot();
	}
	if (slot) {
		slot->mIconSelected->setVisible(true);
		mSelectedSlot = slot;
	}
	fillWeaponInfo(slot);
}

void
Shop::deselectSlot()
{
	mSelectedSlot->mIconSelected->setVisible(false);
	mSelectedSlot.reset();
}

void
Shop::onButtonPurchaseWeapon()
{
	MINIGINE_ASSERT_SOFT(mSelectedSlot);
	MINIGINE_ASSERT_SOFT(mSelectedSlot->mItem);
	MINIGINE_ASSERT_SOFT(mSelectedSlot->mItem->mConfig);
	const WeaponConfig * config = mSelectedSlot->mItem->mConfig;
	if (mInventory->hasEnoughMoney(config->mCost)) {
		mInventory->purchaseWeapon(config);
		mSelectedSlot->mItem.reset(); // Remove item from Shop slot

		reloadItems();

		// Select slot in Inventory that contains the newly purchased item
		auto iter = std::find_if(mSlotsInventory.begin(), mSlotsInventory.end(),
			[config](const SPWeaponSlot & inventorySlot) {
				if (inventorySlot->mItem) {
					return inventorySlot->mItem->mConfig->mName == config->mName;
				}
				else return false;
			});
		if (iter != mSlotsInventory.end()) {
			selectSlot(*iter);
		}
		else {
			MINIGINE_BREAK("Could not find slot with new Item in inventory after purchasing");
		}
	}
	else {
		mSoundNoMoney->play();
	}
}

void
Shop::onButtonPurchaseAmmo()
{
	MINIGINE_ASSERT_SOFT(mSelectedSlot);
	MINIGINE_ASSERT_SOFT(mSelectedSlot->mItem);
	MINIGINE_ASSERT_SOFT(mSelectedSlot->mItem->mConfig);
	const WeaponConfig * config = mSelectedSlot->mItem->mConfig;
	if (mInventory->hasEnoughMoney(config->mAmmoCost)) {
		mInventory->purchaseAmmo(config);
		fillWeaponInfo(mSelectedSlot);
	}
	else {
		mSoundNoMoney->play();
	}
}

SPShopItem
Shop::createShopItem(const WeaponConfig * config, const SPWeaponSlot & slot)
{
	SPShopItem item; item.reset(new ShopItem());
	item->mComplex = GUI_panel(96, 96); {

		// auto colorpad = GUI_colorpad(glm::vec4(1, 1, 0, 1), item->mComplex->getSize());
		// item->mComplex->add(colorpad);

		Minigine::SPImageVO icon;
		icon.reset(new Minigine::ImageVO(config->mIcon, item->mComplex->getSize()));
		item->mComplex->add(icon);
		item->mConfig = config;

		// GUI_StringParams params;
		// params.mString = "Pistol";
		// params.mFontSize = 20;
		// auto name = GUI_string(params);
		// name->setAlignment(0.5f, 0.0f);
		// name->setPosition(glm::vec2(item->mComplex->getWidth() / 2, 0.0f));
		// item->mComplex->add(name);

		mItems.push_back(item);

		moveItemToSlot(item, slot);
	}
	return item;
}

Minigine::SPComplexButton
Shop::createPurchaseButton(PriceButton & out)
{
	Minigine::SPComplexButton buttonPurchase; {
		auto complex = GUI_panel(140, 40);
		complex->setAlignment(0.5f, 0.5f);
		complex->setPosition(complex->getSize() * 0.5f);
		{
			auto colorpad = GUI_colorpad(glm::vec4(0.3f, 0.3f, 0.3f, 1.0f), complex->getSize());
			complex->add(colorpad);
		}
		{
			out.mList = GUI_panel(0, 0);
			out.mList->setAlignment(0.5f, 0.0f);
			out.mList->setPosition(complex->getSize() * 0.5f);
			complex->add(out.mList);

			GUI_StringParams params;
			params.mString = "300";
			params.mFontSize = 30;
			out.mPrice = GUI_string(params);
			out.mPrice->setAlignment(0.0f, 0.5f);
			out.mPrice->setColor(glm::vec4(1.0f, 0.2f, 0.2f, 1.0f));
			out.mList->add(out.mPrice);

			auto coinIcon = GUI_image(SpriteBits::inst()->UI_Coin, glm::vec2(32, 32));
			coinIcon->setAlignment(0.0f, 0.5f);
			coinIcon->setPosition(0.0f, complex->getHeight() * 0.5f);
			out.mList->add(coinIcon);
		}
		buttonPurchase.reset(new Minigine::ComplexButton(complex));
		buttonPurchase->setAlignment(0.5f, 0.0f);
		out.mButton = buttonPurchase;
	}
	return buttonPurchase;
}

void
Shop::updatePriceButton(int price, const PriceButton & button)
{
	button.mPrice->setString(Minigine::to_string(price));
	GUIList::make(button.mList, GUIList::Type::Hor, 5.0f);
}

void
Shop::reloadItems()
{
	mComplexItemsShop->removeAll();
	mComplexItemsInventory->removeAll();
	mItems.clear();

	const std::vector<const WeaponConfig*> list = {
		&WeaponConfigs::inst()->Pistol,
		&WeaponConfigs::inst()->Tec9,
		&WeaponConfigs::inst()->Shotgun,
	};

	int index = 0;
	for (auto & weaponConfig : list) {
		SPWeaponSlot weaponSlot = mSlotsShop.at(index);
		index += 1;

		if (mInventory->hasWeapon(weaponConfig)) {
			continue;
		}

		auto item = createShopItem(weaponConfig, weaponSlot);
		mComplexItemsShop->add(item->mComplex);
	}

	index = 0;
	for (auto & weapon : mInventory->getAllWeapons()) {
		SPWeaponSlot weaponSlot = mSlotsInventory.at(index);
		index += 1;

		auto item = createShopItem(weapon->mConfig, weaponSlot);
		item->mWeapon = weapon;
		mComplexItemsInventory->add(item->mComplex);
	}
}
