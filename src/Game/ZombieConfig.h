#pragma once

#include <minigine/Singleton.h>

class ZombieConfig
{
public:
	std::string mTag;
	float mLevelTime = 0; // How much time this configuration should exist for
	float mHealth = 0;
	float mDamagePerSecond = 0;
	int mMaxZombies = 0;
	float mFillTime = 0; // How much time it takes to spawn zombies [from 0 to mMaxZombies]
	glm::vec2 mMoney;
};

class ZombieConfigs : public Singleton<ZombieConfigs>
{
	std::vector<const ZombieConfig*> List = {
		&Level1, &Level2, &Level3,
	};

public:
	const ZombieConfig * getConfigForTime(float time) {
		float acc = 0.0f;
		for (auto & config : List) {
			acc += config->mLevelTime;
			if (time < acc) {
				return config;
			}
		}
		return List.back();
	}

	const ZombieConfig Level1 = []() {
		ZombieConfig zombie;
		zombie.mTag = "level1";
		zombie.mLevelTime = 15.0f;
		zombie.mHealth = 2.0f;
		zombie.mDamagePerSecond = 20.0f;
		zombie.mMaxZombies = 10;
		zombie.mFillTime = 10.0f;
		zombie.mMoney = glm::vec2(10, 15);
		return zombie;
	}();

	const ZombieConfig Level2 = []() {
		ZombieConfig zombie;
		zombie.mTag = "level2";
		zombie.mLevelTime = 30.0f;
		zombie.mHealth = 4.0f;
		zombie.mDamagePerSecond = 20.0f;
		zombie.mMaxZombies = 10;
		zombie.mFillTime = 10.0f;
		zombie.mMoney = glm::vec2(20, 30);
		return zombie;
	}();

	const ZombieConfig Level3 = []() {
		ZombieConfig zombie;
		zombie.mTag = "level3";
		zombie.mLevelTime = 30.0f;;
		zombie.mHealth = 6.0f;
		zombie.mDamagePerSecond = 30.0f;
		zombie.mMaxZombies = 20;
		zombie.mFillTime = 20.0f;
		zombie.mMoney = glm::vec2(30, 45);
		return zombie;
	}();

};
