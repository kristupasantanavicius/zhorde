#include <Game/UI.h>
#include <minigine/KeyboardInput.h>

UI::UI(SPInventory inventory, SPTimeline timeline, UICallback callback)
: mCallback(callback)
, mInventory(inventory)
, mTimeline(timeline)
{
	mMainMenu.reset(new MainMenu(callback));
	mComponents.setLayerComponent(LO_MAIN_MENU, mMainMenu);

	mHUD.reset(new GameHUD(mInventory));
	mComponents.setLayerComponent(LO_HUD, mHUD);	

	mShop.reset(new Shop(mInventory));
	mComponents.setLayerComponent(LO_SHOP, mShop);

	mGraphView.reset(new GraphView(mTimeline));
	mComponents.setLayerComponent(LO_GRAPHS, mGraphView);
}

void
UI::setPlayerHealth(float health, float maxHealth)
{
	mHUD->setPlayerHealth(health, maxHealth);
}

void
UI::setLevelTime(float time)
{
	mHUD->setLevelTime(time);
}

void
UI::setUIState(UIState state)
{
	mComponents.setLayerComponent(LO_MAIN_MENU, SPComponentBase());
	mComponents.setLayerComponent(LO_HUD, SPComponentBase());
	mComponents.setLayerComponent(LO_SHOP, SPComponentBase());

	switch (state) {
		case UIState::MainMenu:
		{
			mComponents.setLayerComponent(LO_MAIN_MENU, mMainMenu);
		} break;

		case UIState::InGame:
		{
			mComponents.setLayerComponent(LO_HUD, mHUD);
		} break;

		default: MINIGINE_BREAK("UIState is not implemented");
	}

	mState = state;
}

void
UI::showShop()
{
	MINIGINE_ASSERT_SOFT(!mComponents.hasLayerComponent(LO_SHOP));
	mComponents.setLayerComponent(LO_SHOP, mShop);
	mShop->onShow();
}

void
UI::hideShop()
{
	MINIGINE_ASSERT_SOFT(mComponents.hasLayerComponent(LO_SHOP));
	mComponents.setLayerComponent(LO_SHOP, SPComponentBase());
}

void
UI::render(const Minigine::DrawInfo & drawInfo)
{
	mComponents.render(drawInfo);
}

void
UI::update(float fd)
{
	mComponents.update(fd);
}

Minigine::VisibleObject2d::TouchHandleResult
UI::pointerEvent(const Minigine::PointerEvent & event)
{
	return mComponents.pointerEvent(event);
}

