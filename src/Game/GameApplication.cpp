#include <Game/Game.h>
#include <minigine/Application.h>
#include <minigine/KeyboardInput.h>
#include <minigine/sandbox/ShaderManager.h>
#include <minigine/sandbox/ParticlesTextureManager.h>
#include <minigine/TextureManager.h>
#include <minigine/UISounds.h>

void CUIScale_loadPlatformTable(UIScaleLoad * load) { }
void minigineMainCallback() { }
void minigineCrashReportMac() { }
void MINIGINE_LOG_ERROR(const std::string & msg) { }
bool Minigine::Application::canStart() { return true; }
void Minigine::UISounds::buttonSound() { }

class GameApplication : public Minigine::Application
{
	std::shared_ptr<Game> mGame;

public:
	GameApplication(void * details)
	: Application(details)
	{ }

	void
	init() override
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		TextureManager::init();
		Minigine::Sandbox::ParticlesTextureManager::init();
		Minigine::Sandbox::ShaderManager::init();
		initShaders();

		mGame.reset(new Game());
	}

	void
	initShaders()
	{
		Minigine::GLAppContext &GLContext = Minigine::GLAppContext::getInstance();

		Minigine::Sandbox::ShaderManager::createProgram("vo_textured", "resources/shaders/core/vo.vert", "resources/shaders/core/vo.frag");
		Minigine::SPGLSLProgram vo_textured = Minigine::Sandbox::ShaderManager::getProgram("vo_textured");
		// @todo No need to enable/disable vertex attribute arrays, provide a lightweight version of bind which just bind ?
		GLContext.bindProgram(vo_textured);
		glUniform1i(vo_textured->getUniformLocation("texture"), 0);

		Minigine::Sandbox::ShaderManager::createProgram("stringvo", "resources/shaders/core/stringvo.vert", "resources/shaders/core/stringvo.frag");
		Minigine::SPGLSLProgram vo_svo = Minigine::Sandbox::ShaderManager::getProgram("stringvo");
		// @todo No need to enable/disable vertex attribute arrays, provide a lightweight version of bind which just bind ?
		GLContext.bindProgram(vo_svo);
		glUniform1i(vo_svo->getUniformLocation("texture"), 0);

		Minigine::Sandbox::ShaderManager::createProgram("vo_no_texture", "resources/shaders/core/vo_no_texture.vert", "resources/shaders/core/vo_no_texture.frag");
		Minigine::SPGLSLProgram vo_no_textured = Minigine::Sandbox::ShaderManager::getProgram("vo_no_texture");
		// @todo No need to enable/disable vertex attribute arrays, provide a lightweight version of bind which just bind ?
		GLContext.bindProgram(vo_no_textured);	

		Minigine::Sandbox::ShaderManager::createProgram("vo_vertexcolor", "resources/shaders/core/vo_vertexcolor.vert", "resources/shaders/core/vo_vertexcolor.frag");
		Minigine::SPGLSLProgram vo_vertexcolor = Minigine::Sandbox::ShaderManager::getProgram("vo_vertexcolor");
		// @todo No need to enable/disable vertex attribute arrays, provide a lightweight version of bind which just bind ?
		GLContext.bindProgram(vo_vertexcolor);
		glUniform1i(vo_vertexcolor->getUniformLocation("texture"), 0);

		resizeShaders();
	}

	void
	resizeShaders()
	{
		Minigine::GLAppContext &GLContext = Minigine::GLAppContext::getInstance();

		const glm::ivec2 screenSize = Minigine::GLAppContext::getInstance().getScreenSize();

		Minigine::SPGLSLProgram vo_textured = Minigine::Sandbox::ShaderManager::getProgram("vo_textured");
		GLContext.bindProgram(vo_textured);
		glUniform2f(vo_textured->getUniformLocation("screenHalfSize"),static_cast<GLfloat>(screenSize.x/2), static_cast<GLfloat>(screenSize.y/2));

		Minigine::SPGLSLProgram vo_svo = Minigine::Sandbox::ShaderManager::getProgram("stringvo");
		GLContext.bindProgram(vo_svo);
		glUniform2f(vo_svo->getUniformLocation("screenHalfSize"), static_cast<GLfloat>(screenSize.x/2), static_cast<GLfloat>(screenSize.y/2));

		Minigine::SPGLSLProgram vo_no_textured = Minigine::Sandbox::ShaderManager::getProgram("vo_no_texture");
		GLContext.bindProgram(vo_no_textured);	
		glUniform2f(vo_no_textured->getUniformLocation("screenHalfSize"), static_cast<GLfloat>(screenSize.x/2), static_cast<GLfloat>(screenSize.y/2));

		Minigine::SPGLSLProgram vo_vertexcolor = Minigine::Sandbox::ShaderManager::getProgram("vo_vertexcolor");
		GLContext.bindProgram(vo_vertexcolor);
		glUniform2f(vo_vertexcolor->getUniformLocation("screenHalfSize"), static_cast<GLfloat>(screenSize.x/2), static_cast<GLfloat>(screenSize.y/2));
	}
	void
	terminate() override
	{
	}

	void
	windowSizeChangedImpl(int width, int height) override
	{
	}

	void
	update(uint32_t frameTime, float fd) override
	{
		mGame->update(fd);
	}
	void
	render() override
	{
		glClear(GL_COLOR_BUFFER_BIT);
		Minigine::DrawInfo drawInfo;
		drawInfo.color = glm::vec4(1, 1, 1, 1);
		mGame->render(drawInfo);
	}

	void keyPressed(int key) override
	{
		KeyboardInput::instance().keyPressed(key);
		switch (key) {
			case KeyboardInput::KEY_ESCAPE: close(); break;
			default: break;
		}
		mGame->keyPressed(key);
	}
	void keyReleased(int key) override
	{
		KeyboardInput::instance().keyReleased(key);
	}

	void
	pointerEvent(const Minigine::PointerEvent & event) override
	{
		mGame->pointerEvent(event);
	}

	void
	onControllerEvent(const ControllerEvent & event) override
	{
	}

	void
	applicationBackgroundEnter() override
	{
	}
	void
	applicationBackgroundLeave() override
	{
	}

};

Minigine::Application*
createApplication(void * details)
{
	return new GameApplication(details);
}
