#pragma once

#include <vector>
#include <memory>

class Timeline
{
public:
	void setLevelTime(float time) { mLevelTime = time; }
	void recordHealth(float health) { mHealth.push_back(FloatRecord(mLevelTime, health)); }
	void recordZombieKill() { mZombieKills.push_back(mLevelTime); }

	void loadData();
	void saveData();

private:
	float mLevelTime = 0;

	struct FloatRecord {
		FloatRecord(float time, float value) : mTime(time), mValue(value) { }
		float mTime;
		float mValue;
	};

	std::vector<FloatRecord> mHealth;
	std::vector<float> mZombieKills;

};

typedef std::shared_ptr<Timeline> SPTimeline;
