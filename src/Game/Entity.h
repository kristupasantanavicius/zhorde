#pragma once

#include <minigine/ImageVO.h>
#include <array>

typedef uint32_t EntityHandle;

enum class EntityType
{
	None,
	Player,
	Bullet,
	Zombie,
};

class Entity
{
public:
	Entity(EntityType type, EntityHandle handle)
	: mType(type)
	, mHandle(handle)
	{
		init();
	}

	void render(const Minigine::DrawInfo & drawInfo);

	void entityMove(const glm::vec2 & position);
	void entityRotate(float rotation);
	const glm::vec2 & getPosition() { return mPosition; }
	float getRotation() { return mRotation; }
	float getRotationAdjusted();

	Minigine::ImageVO * getImage() { return &mImage; }

	void makeRemoved() { MINIGINE_ASSERT_SOFT(!mRemoved); mRemoved = true; }
	bool isRemoved() { return mRemoved; }

	EntityType getEntityType() { return mType; }
	EntityHandle getEntityHandle() { return mHandle; }

public:
	float mRadius = 0.0f;
	void setDiameter(float diameter) {
		mRadius = diameter / 2;
	}

private:
	void init();

private:
	EntityType mType = EntityType::None;
	EntityHandle mHandle = 0;
	glm::vec2 mPosition;
	float mRotation = 0;
	Minigine::ImageVO mImage;
	bool mRemoved = false;

};

typedef std::shared_ptr<Entity> SPEntity;

#define ENTITY_CTOR(classType, entType) \
public: \
	classType(EntityHandle handle) \
	: Entity(entType, handle) { }

class Player
	: public Entity
{
	ENTITY_CTOR(Player, EntityType::Player)
	Minigine::ImageVO mGun;
	float mHealthMax = 100.0f;
	float mHealth = 100.0f;
	float mPlayerStun = 0;
};

class Bullet
	: public Entity
{
	ENTITY_CTOR(Bullet, EntityType::Bullet)
	float mBulletSpeed = 0.0f;
	float mDamage = 0.0f;
	float mBulletStun = 0;
	float mBulletHealth = 0;

	// _______________________________________________________________________
	// This array allows to check collision with each entity only once.
	// Without it, bullet would probably disappear on first entity collision,
	// since collisions would be calculated each frame for same entity while
	// bullet is penetrating other entity.
	// _______________________________________________________________________
	// Bullet cannot penetrate more entities than this array can hold.
	// When this array is full, bullet will disappear on next Entity collision,
	// it won't matter if it still has HP left.
	std::array<EntityHandle, 5> mSkipEntityCollisions = { };
};

class Zombie
	: public Entity
{
public:
	ENTITY_CTOR(Zombie, EntityType::Zombie)
	int mPushFactor = 0;
	float mHealth = 0.0f;
	float mMaxHealth = 0;
	bool mSeenPositionValid = false;
	glm::vec2 mLastSeenPosition;
	float mWalkTimer = 0.0f;
	int mMoney = 0;
	float mZombieStun = 0;
	float mDamagePerSecond = 0;
};

// Entity can be a raw pointer or smart pointer, so provide functions for both.
#define ENTITY_CONVERSION(funcName, ClassType, entType) \
inline ClassType * funcName(Entity * ent) { \
	if (ent->getEntityType() == entType) return static_cast<ClassType*>(ent); \
	else return nullptr; \
} \
inline ClassType * funcName(SPEntity & ent) { \
	if (ent->getEntityType() == entType) return static_cast<ClassType*>(ent.get()); \
	else return nullptr; \
}

ENTITY_CONVERSION(toPlayer, Player, EntityType::Player)
ENTITY_CONVERSION(toZombie, Zombie, EntityType::Zombie)
ENTITY_CONVERSION(toBullet, Bullet, EntityType::Bullet)
