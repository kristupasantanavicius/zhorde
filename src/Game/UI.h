#pragma once

#include <Game/UICallback.h>
#include <Game/MainMenu.h>
#include <Game/GameHUD.h>
#include <Game/Inventory.h>
#include <Game/Shop.h>
#include <Game/GraphView.h>
#include <EngineEXT/DialogComponent.h>
#include <EngineEXT/ComponentManager.h>

enum class UIState
{
	None,
	MainMenu,
	InGame,
	AfterGame,
};

class UI : public ComponentBase
{
	enum { LO_HUD, LO_SHOP, LO_GRAPHS, LO_MAIN_MENU, };

public:
	UI(SPInventory inventory, SPTimeline timeline, UICallback callback);

	void setPlayerHealth(float health, float maxHealth);
	void setLevelTime(float time);

	void setUIState(UIState state);

	void showShop();
	void hideShop();

private:
	void render(const Minigine::DrawInfo &iDrawInfo) override;
	void update(float fd) override;
	Minigine::VisibleObject2d::TouchHandleResult pointerEvent(const Minigine::PointerEvent & event) override;

private:
	UICallback mCallback;
	ComponentManager mComponents;

	std::shared_ptr<MainMenu> mMainMenu;
	std::shared_ptr<GameHUD> mHUD;
	std::shared_ptr<Shop> mShop;
	std::shared_ptr<GraphView> mGraphView;

	SPInventory mInventory;
	SPTimeline mTimeline;

	UIState mState = UIState::None;

};
