#pragma once

#include <minigine/Easings.h>

class MoneyEffect
{
public:
	int mMoney = 0;

	MoneyEffect(int money, float startIn, glm::vec2 position)
	: mPosition(position)
	, mMoney(money)
	, mStartTimer(startIn)
	{
		mImage.setSprite(SpriteBits::inst()->Money_Ball, glm::vec2(32, 32));
		mImage.setAlignment(0.5f, 0.5f);
	}

	bool
	update(float fd, const glm::vec2 & playerPosition)
	{
		if (mStartTimer > 0.0f) {
			mStartTimer -= fd;
			return false;
		}

		glm::vec2 delta = playerPosition - mPosition;

		if (glm::length(delta) < 25.0f) {
			return true;
		}

		// This wasn't done by lerping the position, because that way
		// when player is close to the orb, it would take the same amount of time
		// to reach the player as if the player was far away.
		// This way, the orb accelerates the same no matter the distance to the player.

		mSpeedTimer += fd;
		const float maxTime = 1.0f;
		if (mSpeedTimer > maxTime) {
			mSpeedTimer = maxTime;
		}
		float speed = 1000.0f * Minigine::ease(Minigine::EaseFunc::IN_SINE, mSpeedTimer, maxTime);

		mPosition += glm::normalize(delta) * speed * fd;

		mImage.setPosition(mPosition);
		return false;
	}

	void
	draw(const Minigine::DrawInfo & drawInfo)
	{
		mImage.draw(drawInfo);
	}

private:
	float mStartTimer = 0.0f;
	glm::vec2 mPosition;
	Minigine::ImageVO mImage;
	float mSpeedTimer = 0.0f;

};

typedef std::shared_ptr<MoneyEffect> SPMoneyEffect;
