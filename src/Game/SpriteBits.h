#pragma once

#include <minigine/Singleton.h>
#include <minigine/TextureManager.h>
#include <minigine/Sprite.h>
#include <vector>

class SpriteBits : public Singleton<SpriteBits>
{
private:
	Minigine::SPSprite
	getSprite(const std::string & name, int gridx, int gridy, int x, int y, int w, int h)
	{
		// Sprite(SPTexture texture, glm::vec2 min, glm::vec2 max, glm::vec2 originalSize, glm::vec2 trimMin, glm::vec2 trimMax)

		auto texture = TextureManager::getTexture(name);

		float gx = gridx / texture->getSize().x;
		float gy = gridy / texture->getSize().y;

		glm::vec2 min(gx * x, gy * y);
		glm::vec2 max(gx * (x + w), gy * (y + h));
		fixSpriteCoordinates(min, max);

		glm::vec2 trimMin;
		glm::vec2 trimMax(1, 1);

		Minigine::SPSprite sprite;
		sprite.reset(new Minigine::Sprite(TextureManager::getTexture(name), min, max, texture->getSize(), trimMin, trimMax));
		return sprite;
	}

public:
	SpriteBits()
	{
	}

	Minigine::SPSprite WTF = getSprite("resources/textures/spritesheet.png", 8, 8, 30, 31, 1, 1);

	Minigine::SPSprite UI_SlotSelected = getSprite("resources/textures/spritesheet.png", 16, 16, 1, 0, 1, 1);
	Minigine::SPSprite UI_Coin = getSprite("resources/textures/spritesheet.png", 16, 16, 2, 1, 1, 1);

	Minigine::SPSprite Tile_Grass = getSprite("resources/textures/spritesheet.png", 16, 16, 3, 0, 1, 1);
	Minigine::SPSprite Tile_Rock = getSprite("resources/textures/spritesheet.png", 16, 16, 0, 1, 1, 1);

	Minigine::SPSprite Player_Pose_Hand = getSprite("resources/textures/player.png", 32, 32, 0, 0, 1, 1);
	Minigine::SPSprite Player_Pose_Shoulder = getSprite("resources/textures/player.png", 32, 32, 1, 0, 1, 1);
	Minigine::SPSprite Player_Pose_Heavy = getSprite("resources/textures/player.png", 32, 32, 2, 0, 1, 1);

	Minigine::SPSprite Zombie_1 = getSprite("resources/textures/zombie.png", 32, 32, 0, 1, 1, 1);
	Minigine::SPSprite Zombie_2 = getSprite("resources/textures/zombie.png", 32, 32, 1, 1, 1, 1);
	Minigine::SPSprite Zombie_3 = getSprite("resources/textures/zombie.png", 32, 32, 2, 1, 1, 1);
	Minigine::SPSprite Zombie_4 = getSprite("resources/textures/zombie.png", 32, 32, 3, 1, 1, 1);

	Minigine::SPSprite Gun_Pistol = getSprite("resources/textures/weapons.png", 32, 32, 1, 0, 1, 1);
	Minigine::SPSprite Gun_Tec9 = getSprite("resources/textures/weapons.png", 32, 32, 1, 1, 1, 1);
	Minigine::SPSprite Gun_Shotgun = getSprite("resources/textures/weapons.png", 32, 32, 0, 2, 1, 1);
	Minigine::SPSprite Gun_Rifle = getSprite("resources/textures/weapons.png", 16, 32, 0, 1, 1, 1);
	Minigine::SPSprite Gun_MachineGun = getSprite("resources/textures/weapons.png", 16, 32, 1, 1, 1, 1);

	Minigine::SPSprite Icon_Pistol = getSprite("resources/textures/spritesheet.png", 16, 16, 0, 4, 1, 1);
	Minigine::SPSprite Icon_Tec9 = getSprite("resources/textures/spritesheet.png", 16, 16, 1, 4, 1, 1);
	Minigine::SPSprite Icon_Shotgun = getSprite("resources/textures/spritesheet.png", 16, 16, 2, 4, 1, 1);
	Minigine::SPSprite Icon_Rifle = getSprite("resources/textures/spritesheet.png", 16, 16, 3, 4, 1, 1);
	Minigine::SPSprite Icon_MachineGun = getSprite("resources/textures/spritesheet.png", 16, 16, 4, 4, 1, 1);

	Minigine::SPSprite Bullet_Round = getSprite("resources/textures/weapons.png", 8, 8, 0, 2, 1, 1);
	Minigine::SPSprite Bullet_Shrapnel = getSprite("resources/textures/weapons.png", 8, 8, 1, 2, 1, 1);

	Minigine::SPSprite Money_Ball = getSprite("resources/textures/spritesheet.png", 8, 8, 2, 4, 1, 1);

	Minigine::SPSprite Effect_Shoot_F1 = getSprite("resources/textures/spritesheet.png", 16, 16, 10, 3, 1, 1);
	Minigine::SPSprite Effect_Shoot_F2 = getSprite("resources/textures/spritesheet.png", 16, 16, 11, 3, 1, 1);
	Minigine::SPSprite Effect_Shoot_F3 = getSprite("resources/textures/spritesheet.png", 16, 16, 12, 3, 1, 1);
	Minigine::SPSprite Effect_Shoot_F4 = getSprite("resources/textures/spritesheet.png", 16, 16, 13, 3, 1, 1);

	Minigine::SPSprite Effect_Zombie_Death_F1 = getSprite("resources/textures/spritesheet.png", 16, 16, 0, 3, 1, 1);
	Minigine::SPSprite Effect_Zombie_Death_F2 = getSprite("resources/textures/spritesheet.png", 16, 16, 1, 3, 1, 1);
	Minigine::SPSprite Effect_Zombie_Death_F3 = getSprite("resources/textures/spritesheet.png", 16, 16, 2, 3, 1, 1);
	Minigine::SPSprite Effect_Zombie_Death_F4 = getSprite("resources/textures/spritesheet.png", 16, 16, 3, 3, 1, 1);
	Minigine::SPSprite Effect_Zombie_Death_F5 = getSprite("resources/textures/spritesheet.png", 16, 16, 4, 3, 1, 1);
	Minigine::SPSprite Effect_Zombie_Death_F6 = getSprite("resources/textures/spritesheet.png", 16, 16, 5, 3, 1, 1);

	Minigine::SPSprite Effect_Bullet_Wall_F1 = getSprite("resources/textures/spritesheet.png", 16, 16, 6, 3, 1, 1);
	Minigine::SPSprite Effect_Bullet_Wall_F2 = getSprite("resources/textures/spritesheet.png", 16, 16, 7, 3, 1, 1);
	Minigine::SPSprite Effect_Bullet_Wall_F3 = getSprite("resources/textures/spritesheet.png", 16, 16, 8, 3, 1, 1);
	Minigine::SPSprite Effect_Bullet_Wall_F4 = getSprite("resources/textures/spritesheet.png", 16, 16, 9, 3, 1, 1);

	std::vector<Minigine::SPSprite> Effect_Shoot = { Effect_Shoot_F1, Effect_Shoot_F2, Effect_Shoot_F3, Effect_Shoot_F4, };
	std::vector<Minigine::SPSprite> Effect_Zombie_Death = { Effect_Zombie_Death_F1, Effect_Zombie_Death_F2, Effect_Zombie_Death_F3, Effect_Zombie_Death_F4, Effect_Zombie_Death_F5, Effect_Zombie_Death_F6, };
	std::vector<Minigine::SPSprite> Effect_Bullet_Wall = { Effect_Bullet_Wall_F1, Effect_Bullet_Wall_F2, Effect_Bullet_Wall_F3, Effect_Bullet_Wall_F4, };

};
