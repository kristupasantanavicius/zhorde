#include <Game/Timeline.h>
#include <Game/Console.h>
#include <minigine/DataStorage.h>
#include <minigine/utils/JsonObject.h>

void
Timeline::loadData()
{
	mHealth.clear();
	mZombieKills.clear();

	if (!Minigine::DataStorage::exist("timeline_data_debug")) {
		CONSOLE_ERROR("timeline", "timeline_data_debug doesn't exist");
		return;
	}

	std::string data = Minigine::DataStorage::getString("timeline_data_debug");
	SPJsonObject root = Minigine::JsonParser::parse(data.c_str());
	MINIGINE_ASSERT_SOFT(root);

	if (!root) {
		CONSOLE_ERROR("timeline", "timeline_data_debug failed to parse");
		return;
	}

	const rapidjson::Value & json = root->Value();

	const rapidjson::Value & healths = json["healths"];
	for (auto iter = healths.Begin(); iter != healths.End(); /*EMPTY*/ ) {
		float time = (*iter).GetFloat();
		++iter;
		MINIGINE_ASSERT_SOFT(iter != healths.End());
		float value = (*iter).GetFloat();
		++iter;

		mHealth.push_back(FloatRecord(time, value));		
	}

	const rapidjson::Value & kills = json["kills"];
	for (auto iter = kills.Begin(); iter != kills.End(); ++iter) {
		float time = (*iter).GetFloat();
		mZombieKills.push_back(time);
	}
}

void
Timeline::saveData()
{
	SPJsonObject root = Minigine::JsonObject::create();

	JsonObject healths(*root, JsonObject::Array);
	for (auto & record : mHealth) {
		healths.pushBackDouble(record.mTime);
		healths.pushBackDouble(record.mValue);
	}
	root->setObject("healths", healths);

	JsonObject kills(*root, JsonObject::Array);
	for (auto & time : mZombieKills) {
		kills.pushBackDouble(time);
	}
	root->setObject("kills", kills);

	std::string data = root->toString();
	Minigine::DataStorage::setString("timeline_data_debug", data);
}
