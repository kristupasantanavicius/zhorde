#include <Game/PlayerHUD.h>
#include <EngineEXT/UIHelpers/GUIHelper.h>

static const glm::vec2 Reloading_Bar_Size(160.0f, 10.0f);

void
PlayerHUD::init()
{
	mComplex = GUI_panel(128, 128);
	mComplex->setAlignment(0.5f, 0.5f);

	// auto background = GUI_colorpad(glm::vec4(1, 0, 0, 1), glm::vec2(50, 50));
	// mComplex->add(background);

	// glm::vec2 size, int sectors, bool filled, float lineWidth, glm::vec4 lineColor, glm::vec4 fillColor, float range = 360.0f

	mReloadingComplex = GUI_panel(mComplex->getSize());
	mReloadingComplex->setVisible(false);
	mComplex->add(mReloadingComplex);

	GUI_StringParams params;
	params.mString = "Reloading";
	params.mFontSize = 30;
	params.mColor = glm::vec4(0.8f, 0.1f, 0.1f, 1.0f);
	mStringReloading = GUI_string(params);
	mStringReloading->setAlignment(0.5f, 0.5f);
	mStringReloading->setPosition(mComplex->getSize() * 0.5f);
	mReloadingComplex->add(mStringReloading);

	glm::vec2 reloadBarBorder(2, 2);
	auto reloadBarBackground = GUI_colorpad(glm::vec4(0.6f, 0, 0, 1), Reloading_Bar_Size + reloadBarBorder * 2.0f);
	reloadBarBackground->setPosition(mComplex->getSize() * 0.5f + glm::vec2(-Reloading_Bar_Size.x/2, -30));
	mReloadingComplex->add(reloadBarBackground);

	mReloadingBar = GUI_colorpad(glm::vec4(1, 0, 0, 1), Reloading_Bar_Size);
	mReloadingBar->setPosition(reloadBarBackground->getPosition() + reloadBarBorder);
	mReloadingComplex->add(mReloadingBar);

	mEllipse.reset(new Minigine::Ext::EllipseVO(mComplex->getSize(), 30));
	mEllipse->setLineWidth(4);
	mEllipse->setPosition(mComplex->getSize() * 0.5f);
	mEllipse->setColor(glm::vec4(1, 0, 0, 0.5f));
	mEllipse->setOrder(-1);
	mComplex->add(mEllipse);

	mStun.reset(new Minigine::Ext::EllipseVO(mComplex->getSize() * 0.8f, 30));
	mStun->setLineWidth(4);
	mStun->setPosition(mComplex->getSize() * 0.5f);
	mStun->setColor(glm::vec4(1, 1, 0, 1));
	mStun->setOrder(-1);
	mComplex->add(mStun);
}

void
PlayerHUD::setPosition(const glm::vec2 & position)
{
	mComplex->setPosition(position);
}

void
PlayerHUD::setHealth(float health, float maxHealth)
{
	float range = (health / maxHealth) * 360.0f;
	mEllipse->makeEllipse(mComplex->getSize(), 30, range);
}

void
PlayerHUD::setStun(float stun, float max)
{
	if (stun < 0) {
		stun = 0;
	}
	if (stun > max) {
		stun = max;
	}
	float range = (stun / max) * 360.0f;
	mStun->makeEllipse(mStun->getSize(), 30, range);
	mStun->setRotation(90 - range/2);
}

void
PlayerHUD::setReloading(bool reloading)
{
	mReloadingComplex->setVisible(reloading);
}

void
PlayerHUD::setReloadingTime(float time, float max)
{
	mReloadingBar->setWidth(Reloading_Bar_Size.x * (time / max));
}

void
PlayerHUD::update(float fd)
{
	mAnimation += fd;
	mEllipse->setRotation(60.0f * mAnimation);
}

void
PlayerHUD::draw(const Minigine::DrawInfo & drawInfo)
{
	mComplex->draw(drawInfo);
}
