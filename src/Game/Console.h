
#pragma once

#include <string>
#include <vector>

enum class ConsoleLogLevel
{
	Info,
	Done,
	Warning,
	Error,
};

// Implement this function wherever you want, on GameClient
// its implemented to display text on screen. On GameBot, it does nothing.
extern void CONSOLE_ECHO_func(const std::string & filter, const std::string & str, ConsoleLogLevel level);

// Echo with no Tag.
inline void CONSOLE_ECHO(const std::string & str) {
	CONSOLE_ECHO_func("NO_FILTER", str, ConsoleLogLevel::Info);
}

inline void CONSOLE_INFO(const std::string & filter, const std::string & msg) {
	CONSOLE_ECHO_func(filter, msg, ConsoleLogLevel::Info);
}
inline void CONSOLE_DONE(const std::string & filter, const std::string & msg) {
	CONSOLE_ECHO_func(filter, msg, ConsoleLogLevel::Done);
}
inline void CONSOLE_WARNING(const std::string & filter, const std::string & msg) {
	CONSOLE_ECHO_func(filter, msg, ConsoleLogLevel::Warning);
}
inline void CONSOLE_ERROR(const std::string & filter, const std::string & msg) {
	CONSOLE_ECHO_func(filter, msg, ConsoleLogLevel::Error);
}

struct ConsoleMessage
{
	ConsoleLogLevel level;
	std::string text;
};

void ConsoleFlushMessages(std::vector<ConsoleMessage> & out);
