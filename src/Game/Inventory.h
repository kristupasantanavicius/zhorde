#pragma once

#include <Game/Weapon.h>
#include <Game/InventoryListener.h>

class Weapon
{
public:
	const WeaponConfig * mConfig = nullptr;
	int mAmmoInMagazine = 0;
	int mAmmoInBackpack = 0;

};

typedef std::shared_ptr<Weapon> SPWeapon;

class Inventory
{
public:
	Inventory();

	void clearInventory();

	void makeShot();
	void reloadCurrentWeapon();
	void purchaseWeapon(const WeaponConfig * config);
	void purchaseAmmo(const WeaponConfig * config);
	void addMoney(int money);

	bool hasAmmoInBackpack();
	bool magazineIsOutOfAmmo();
	bool magazineIsFull();

	void selectNextWeapon();
	void selectPreviousWeapon();

	bool hasWeapon(const WeaponConfig * config) { return getWeaponByConfig(config) != nullptr; }
	const Weapon * getWeaponByConfig(const WeaponConfig * config) { return editWeaponByConfig(config); }
	const Weapon * getCurrentWeapon() { return getWeapon(); }

	const std::vector<SPWeapon> & getAllWeapons() { return mWeapons; }

	bool hasEnoughMoney(int req);
	int getMoney();

	void addInventoryListener(InventoryListener & listener);
	void removeInventoryListener(InventoryListener & listener);

private:
	void createWeapon(const WeaponConfig * config);
	void reloadWeapon(Weapon * weapon);
	void addDefaultWeapon();
	Weapon * getWeapon();
	Weapon * editWeaponByConfig(const WeaponConfig * config);

	void notifyAmmo();
	void notifyMoney();

private:
	int mSelected = -1;
	std::vector<SPWeapon> mWeapons;
	std::vector<InventoryListener*> mListeners;
	int mMoney = 0;

};

typedef std::shared_ptr<Inventory> SPInventory;
