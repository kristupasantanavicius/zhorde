#pragma once

#include <Game/Inventory.h>
#include <EngineEXT/DialogComponent.h>
#include <minigine/ComplexObject.h>
#include <minigine/ImageVO.h>
#include <minigine/StringVO.h>
#include <minigine/Sound.h>
#include <minigine/ui/ComplexButton.h>

class ShopItem
{
public:
	Minigine::SPComplexObject mComplex;
	const WeaponConfig * mConfig = nullptr;
	SPWeapon mWeapon;
};
typedef std::shared_ptr<ShopItem> SPShopItem;

class Shop : public DialogComponent
{
public:
	Shop(SPInventory inventory)
	: mInventory(inventory)
	{ init(); }

	void onShow();

private:
	void init();
	void reloadItems();

	struct WeaponSlot
	{
		Minigine::SPComplexObject mComplex;
		Minigine::SPVisibleObject mIconSelected;
		SPShopItem mItem;
	};
	typedef std::shared_ptr<WeaponSlot> SPWeaponSlot;
	SPWeaponSlot createWeaponSlot(int index, bool colorOther);

	struct PriceButton
	{
		Minigine::SPComplexButton mButton;
		Minigine::SPComplexObject mList;
		Minigine::SPStringVO mPrice;
	};
	void updatePriceButton(int price, const PriceButton & button);

	struct WeaponInfo
	{
		Minigine::SPImageVO mIcon;
		Minigine::SPStringVO mName;

		PriceButton mButtonBuyWeapon;
		PriceButton mButtonBuyAmmo;

		Minigine::SPComplexObject mComplexAmmo;
		Minigine::SPStringVO mStringAmmoAmount;
		Minigine::SPStringVO mStringAmmoGain;
	};

	void moveItemToSlot(const SPShopItem & item, const SPWeaponSlot & slot);
	void onSlotClick(const SPWeaponSlot & slot);
	void selectSlot(const SPWeaponSlot & slot);
	void deselectSlot();
	void fillWeaponInfo(const SPWeaponSlot & slot);
	void onButtonPurchaseWeapon();
	void onButtonPurchaseAmmo();
	SPShopItem createShopItem(const WeaponConfig * config, const SPWeaponSlot & slot);
	Minigine::SPComplexButton createPurchaseButton(PriceButton & out);

private:
	SPInventory mInventory;
	std::vector<SPShopItem> mItems;
	std::vector<SPWeaponSlot> mSlotsShop, mSlotsInventory, mSlotsAll;
	Minigine::SPComplexObject mComplexItemsShop, mComplexItemsInventory;
	SPWeaponSlot mSelectedSlot;
	WeaponInfo mInfo;
	Minigine::SPSound mSoundNoMoney;

};
