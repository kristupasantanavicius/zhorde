
#include <Game/ConsoleComponent.h>
#include <minigine/animation/AnimationFade.h>
#include <minigine/animation/AnimationCanvasVO.h>
#include <minigine/UIScale.h>
#include <fstream>

#include <iomanip>
#include <ctime>
#include <sstream>

static const float ROW_HEIGHT = 20;

ConsoleComponent::ConsoleComponent()
{
	mComplex.reset(new Minigine::ComplexObject(0, 0)); {
		float uiScale = UIScale()->getScaleRaw("");
		mComplex->setPosition(glm::vec2(50, 50) * uiScale);
		mComplex->enableUIScaling();
		
		addToScreen(mComplex);
	}

	mBackground.reset(new Minigine::ColorPadVO(glm::vec4(0, 0, 0, 0.3f), glm::vec2(0, 0)));
	mComplex->add(mBackground);
}

void ConsoleComponent::update(float fd)
{
	DialogComponent::update(fd);

	std::vector<ConsoleMessage> messages;
	ConsoleFlushMessages(messages);
	for (auto & message : messages) {
		addMessage(message.text, message.level);
	}

	for (auto iter = mEntries.begin(); iter != mEntries.end(); /**/ ) {
		ConsoleEntry & entry = *iter;
		
		entry.mHideAnimations.update(fd);
		
		entry.mTime += fd;
		
		const float max = 20.0f;
		if (entry.mTime >= max && !entry.mHidden) {
			entry.mHideAnimations.start();
			entry.mHidden = true;
		}
		
		if (entry.mTime >= max && !entry.mHideAnimations.isRunning()) {
			mComplex->remove(entry.mString);
			iter = mEntries.erase(iter);
			updateBackground();
		}
		else {
			++iter;
		}
		
	}
}

void ConsoleComponent::addMessage(const std::string & str, ConsoleLogLevel level)
{
	ConsoleEntry entry;
	entry.mTime = 0.0f;
	entry.mHidden = false;

	glm::vec4 color = glm::vec4(1, 1, 1, 1);
	switch (level) {
		case ConsoleLogLevel::Info: color = glm::vec4(1, 1, 1, 1); break;
		case ConsoleLogLevel::Done: color = glm::vec4(0.6f, 1.0f, 0.3f, 1); break;
		case ConsoleLogLevel::Warning: color = glm::vec4(1, 0.8f, 0.2f, 1); break;
		case ConsoleLogLevel::Error: color = glm::vec4(1.0f, 0.4f, 0.4f, 1); break;
	}
	
	entry.mString = Minigine::SPStringVO(new Minigine::StringVO(
		str,
		Minigine::FontEngine::getFont("resources/fonts/VeraMono-Bold.ttf", 15),
		color
	));
	mComplex->add(entry.mString);
	
	auto fade = new Minigine::AnimationFade(entry.mString, 1, 0);
	entry.mHideAnimations.add(*fade);
	
	mEntries.insert(mEntries.begin(), entry);
	
	for (int i = 0; i < (int)mEntries.size(); i++) {
		mEntries[i].mString->setPosition(glm::vec2(0, i * ROW_HEIGHT));
	}

	updateBackground();

	// Write to log file
	
	// Current date time string
	auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y-%m-%d %H-%M-%S");
	std::string dateTime = oss.str();

	std::ofstream logFile;
	logFile.open("console_log.txt", std::ios_base::app);
	logFile << dateTime << ": " << str << std::endl;
}

void
ConsoleComponent::updateBackground()
{
	float maxWidth = 50;
	for (int i = 0; i < (int)mEntries.size(); i++) {
		maxWidth = std::max(maxWidth, mEntries[i].mString->getWidth());
	}

	const float backgroundBorder = 20.0f;
	float backgroundWidth = maxWidth + backgroundBorder;
	float backgroundHeight = ROW_HEIGHT * mEntries.size() + backgroundBorder;
	mBackground->setSize(glm::vec2(backgroundWidth, backgroundHeight));
	mBackground->setPosition(-glm::vec2(backgroundBorder/2));
}
