#pragma once

#include <functional>

class UICallback
{
public:
	std::function<void()> cStartGame;
	std::function<void()> cExitGame;

};
