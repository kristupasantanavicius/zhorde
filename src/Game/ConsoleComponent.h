
#pragma once

#include <Game/Console.h>
#include <EngineEXT/DialogComponent.h>
#include <minigine/StringVO.h>
#include <minigine/ColorPadVO.h>
#include <minigine/ComplexObject.h>
#include <minigine/animation/AnimationBundle.h>

class ConsoleComponent : public DialogComponent
{
public:
	ConsoleComponent();

	void update(float fd) override;

private:
	void addMessage(const std::string & str, ConsoleLogLevel level);
	void updateBackground();

private:
	struct ConsoleEntry
	{
		Minigine::SPStringVO mString;
		float mTime;
		Minigine::AnimationBundle mHideAnimations;
		bool mHidden;
	};

	std::vector<ConsoleEntry> mEntries;
	Minigine::SPComplexObject mComplex;
	Minigine::SPColorPadVO mBackground;

};
