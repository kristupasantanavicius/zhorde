#pragma once

#include <Game/Inventory.h>
#include <EngineEXT/DialogComponent.h>
#include <minigine/ColorPadVO.h>
#include <minigine/StringVO.h>

class GameHUD
	: public DialogComponent
	, public InventoryListener
{
public:
	GameHUD(SPInventory inventory) : mInventory(inventory) { init(); }
	~GameHUD();

	void setPlayerHealth(float health, float maxHealth);
	void setLevelTime(float levelTime);

	// Inventory Listener
	void inventoryAmmoChanged(int magazine, int backpack) override;
	void inventoryMoneyChanged(int coins) override;

private:
	void init();

private:
	SPInventory mInventory;
	Minigine::SPColorPadVO mHealthBackground;
	Minigine::SPColorPadVO mHealthBar;
	Minigine::SPStringVO mAmmo;
	Minigine::SPStringVO mCoins;
	Minigine::SPStringVO mLevelTime;

};
