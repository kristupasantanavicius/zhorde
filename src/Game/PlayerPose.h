#pragma once

#include <Game/SpriteBits.h>
#include <minigine/Asserts.h>

enum class PlayerPose
{
	None = 0,
	Hand,
	Shoulder,
	Heavy,
};

inline Minigine::SPSprite
PlayerPoseGetSprite(PlayerPose pose)
{
	switch (pose) {
		case PlayerPose::Hand: return SpriteBits::inst()->Player_Pose_Hand;
		case PlayerPose::Shoulder: return SpriteBits::inst()->Player_Pose_Shoulder;
		case PlayerPose::Heavy: return SpriteBits::inst()->Player_Pose_Heavy;
		default: {
			MINIGINE_BREAK("No sprite for player pose.");
			return SpriteBits::inst()->Player_Pose_Hand;
		}
	}
}
