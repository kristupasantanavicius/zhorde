#pragma once

#include <minigine/TimeManagement.h>
#include <minigine/Log.h>

class Timer
{
public:
	Timer()
	: mTime(Minigine::Timestamp::monotonicElapsedTimeMillis())
	{ }

	void
	internal_step(const char * file, int line, const char * function)
	{
		uint64_t current = Minigine::Timestamp::monotonicElapsedTimeMillis();
		LOGI("TIMER", "%s:%s():%d = %dms", file, function, line, (int)(current-mTime));
		mTime = current;
	}

private:
	uint64_t mTime = 0;

};

#define TIMER_STEP(timer) timer.internal_step(__FILE__, __LINE__, __FUNCTION__)
