#pragma once

#include <Game/Level.h>
#include <Game/UI.h>
#include <Game/LevelCallback.h>
#include <Game/UICallback.h>
#include <Game/Inventory.h>
#include <Game/Timeline.h>
#include <EngineEXT/ComponentManager.h>
#include <minigine/VisibleObject.h>

class Game
{
	enum { LO_LEVEL, LO_UI, LO_CONSOLE, };

public:
	Game();

	void update(float fd);
	void render(const Minigine::DrawInfo & drawInfo);
	void keyPressed(int key);
	void pointerEvent(const Minigine::PointerEvent & event);

private:
	void startGame();
	void playerDied();

	void closeShop();
	void refreshLevelPaused();

private:
	std::shared_ptr<Level> mLevel;
	std::shared_ptr<UI> mUI;
	ComponentManager mComponents;

	LevelCallback mLevelCallback;
	UICallback mUICallback;

	SPInventory mInventory;
	SPTimeline mTimeline;

	bool mInLevel = false;
	bool mInShop = false;
	bool mInGraphs = false;

};
