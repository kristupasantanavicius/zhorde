#include <Game/Entity.h>
#include <minigine/TextureManager.h>
#include <minigine/utils/Math.h>

void
Entity::init()
{
}

void
Entity::render(const Minigine::DrawInfo & drawInfo)
{
	mImage.draw(drawInfo);
}

void
Entity::entityMove(const glm::vec2 & position)
{
	mPosition = position;
	mImage.setPosition(position);
}

void
Entity::entityRotate(float rotation)
{
	mRotation = rotation;

	mImage.setRotation(Minigine::toDegree(getRotationAdjusted()));
}

float
Entity::getRotationAdjusted()
{
	// Assets are facing bottom-up, but they should be facing left-right.
	return mRotation - Minigine::PI_2;
}
