#include <Game/Game.h>
#include <Game/DebugManifest.h>
#include <Game/ConsoleComponent.h>
#include <minigine/KeyboardInput.h>

Game::Game()
{
	mInventory.reset(new Inventory());
	mTimeline.reset(new Timeline());

	mUICallback.cStartGame = [this]() { startGame(); };
	mUICallback.cExitGame = [this]() { };

	mUI.reset(new UI(mInventory, mTimeline, mUICallback));
	mComponents.setLayerComponent(LO_UI, mUI);

#ifdef DEBUG
	mComponents.setLayerComponent(LO_CONSOLE, std::shared_ptr<ConsoleComponent>(new ConsoleComponent()));
#endif

	mUI->setUIState(UIState::MainMenu);	

	Minigine::Sound::muteAll(true);
}

void
Game::startGame()
{
	mLevelCallback.cSetHealth = [this](float health, float maxHealth) { mUI->setPlayerHealth(health, maxHealth); };
	mLevelCallback.cSetLevelTime = [this](float time) { mUI->setLevelTime(time); };
	mLevelCallback.cPlayerDead = [this]() { playerDied(); };

	mInventory->clearInventory();

	mLevel.reset(new Level(mInventory, mTimeline, mLevelCallback));

	mUI->setUIState(UIState::InGame);
	mComponents.setLayerComponent(LO_LEVEL, mLevel);

	mInLevel = true;
}

void
Game::playerDied()
{
	mUI->setUIState(UIState::AfterGame);
	mInGraphs = true;
	refreshLevelPaused();

	// mComponents.hideLayer(LO_LEVEL);

	mInLevel = false;
	closeShop();

	mTimeline->saveData();
}

void
Game::closeShop()
{
	if (mInShop) {
		mInShop = false;
		mUI->hideShop();
		refreshLevelPaused();
	}
}

void
Game::refreshLevelPaused()
{
	mLevel->setLevelPaused(mInShop || mInGraphs);
}

void
Game::update(float fd)
{
	mComponents.update(fd);
}

void
Game::render(const Minigine::DrawInfo & drawInfo)
{
	mComponents.render(drawInfo);
}

void
Game::keyPressed(int key)
{
	switch (key) {
		case KeyboardInput::KEY_C: DebugManifest::inst()->toggleDisplayCollisionMeshes(); break;

		case KeyboardInput::KEY_B:
		{
			if (mInLevel) {
				if (!mInShop) {
					mInShop = true;
					mUI->showShop();
					refreshLevelPaused();
				}
				else {
					closeShop();
				}
			}
		} break;

		default: break;
	}
	mLevel->keyPressed(key);
}

void
Game::pointerEvent(const Minigine::PointerEvent & event)
{
	mComponents.pointerEvent(event);
}
