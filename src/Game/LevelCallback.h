#pragma once

#include <functional>

class LevelCallback
{
public:
	std::function<void(float health, float maxHealth)> cSetHealth;
	std::function<void(float time)> cSetLevelTime;
	std::function<void()> cPlayerDead;

};
