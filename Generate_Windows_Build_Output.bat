@echo off

set ABS_PATH=%1
set TARGET=%2

echo Running %ABS_PATH%/Generate_Windows_Build_Output.bat
echo Project_Target=%TARGET%

cd %ABS_PATH%

set TOOLS=minigine\build\msvc2012\tools
set BLINK_SYNC=%TOOLS%\blinksync

set ARCHIVE=build\archive\%TARGET%

%BLINK_SYNC% resources build\archive\%TARGET%\resources
xcopy /D /Y build\%TARGET%\zhorde.exe  %ARCHIVE%
xcopy /D /Y minigine\externals\fmod-4.44.17\bin\win32\fmod.dll  %ARCHIVE%
xcopy /D /Y minigine\externals\SDL\vc\lib\x86\SDL2.dll  %ARCHIVE%
