attribute highp vec3 aVertexPositionFrame1;
attribute highp vec3 aVertexPositionFrame2;
attribute lowp vec2  aVertexTexCoord;

uniform highp mat4  uMVPMatrix;
uniform highp float uBlendFactor;
varying lowp vec2   fragTexCoord;

void main() {
	fragTexCoord = aVertexTexCoord;
	vec3 blendedPosition = aVertexPositionFrame1 * (1.0 - uBlendFactor) + aVertexPositionFrame2 * uBlendFactor;
	gl_Position = uMVPMatrix * vec4(blendedPosition, 1.0);
}