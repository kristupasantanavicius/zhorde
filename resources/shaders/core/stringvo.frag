varying lowp vec2 fragTexcoord;

uniform lowp vec4 objectColor;
uniform lowp sampler2D texture;

void main() {	
	gl_FragColor = vec4(objectColor.rgb,texture2D(texture, fragTexcoord).a*objectColor.a);	
}