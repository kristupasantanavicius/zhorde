varying lowp vec2 fragTexCoord;

uniform lowp sampler2D texture;

void main() {	
	gl_FragColor = texture2D(texture, fragTexCoord);	
}