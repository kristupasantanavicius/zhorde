attribute highp vec2 vertexPosition;
attribute highp vec2 vertexTexcoord;
attribute lowp vec4 vertexColor;

uniform highp mat3 objectMatrix;
uniform highp vec2 screenHalfSize;

varying lowp vec2 fragTexcoord;
varying lowp vec4 fragColor;

void main() {
	vec2 resPos = (objectMatrix*vec3(vertexPosition, 1)).xy/screenHalfSize-vec2(1.0, 1.0);	
	//vec2 resPos = (vertexPosition+vec2(objectMatrix[2][0],objectMatrix[2][1]))/screenHalfSize-vec2(1.0, 1.0);	
	fragTexcoord = vertexTexcoord;	
	fragColor = vertexColor;
	gl_Position = vec4(resPos, 0.0, 1.0);
}