attribute highp 	vec3 aVertexPosition;
attribute lowp 		vec2 aVertexTexCoord;
attribute mediump 	vec4 aMatrixIndices;
attribute lowp 		vec4 aMatrixWeights;

uniform highp mat4 uMatrixPalette[32];
uniform highp mat4 uMVPMatrix;

varying lowp vec2 fragTexCoord;

void skinPosition(in vec4 position, float weight, int index, out vec4 skinnedPosition) {
	skinnedPosition += (uMatrixPalette[index] * position) * weight;
}

void applySkinning(in vec4 position, out vec4 skinnedPosition) {
	skinnedPosition = vec4(0.0);

	skinPosition(position, aMatrixWeights[0], int(aMatrixIndices[0]), skinnedPosition);
	skinPosition(position, aMatrixWeights[1], int(aMatrixIndices[1]), skinnedPosition);
	skinPosition(position, aMatrixWeights[2], int(aMatrixIndices[2]), skinnedPosition);
	skinPosition(position, aMatrixWeights[3], int(aMatrixIndices[3]), skinnedPosition);
}

void main() {
	fragTexCoord = aVertexTexCoord;
	vec4 vertex = vec4(aVertexPosition, 1.0);
	vec4 skinnedVertex;
	applySkinning(vertex, skinnedVertex);
	skinnedVertex.w = 1.0;
	gl_Position = uMVPMatrix * skinnedVertex;
}

