attribute highp vec3 aVertexPosition;
attribute lowp vec2 aVertexTexCoord;

uniform highp mat4 uMVPMatrix;

varying lowp vec2 fragTexCoord;

void main() {
	fragTexCoord = aVertexTexCoord;	
	gl_Position = uMVPMatrix * vec4(aVertexPosition, 1.0);
}