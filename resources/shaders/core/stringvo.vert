attribute highp vec2 vertexPosition;
attribute highp vec2 vertexTexcoord;

uniform highp mat3 objectMatrix;
uniform highp vec2 screenHalfSize;

varying lowp vec2 fragTexcoord;

void main() {
	vec2 resPos = (objectMatrix*vec3(vertexPosition, 1.0)).xy/screenHalfSize-vec2(1.0, 1.0);
	fragTexcoord = vertexTexcoord;	
	gl_Position = vec4(resPos, 0.0, 1.0);
}