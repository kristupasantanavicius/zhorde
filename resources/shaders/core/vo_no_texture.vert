attribute highp vec2 vertexPosition;

uniform highp mat3 objectMatrix;
uniform highp vec2 screenHalfSize;

void main() {
	vec2 resPos = (objectMatrix*vec3(vertexPosition, 1.0)).xy/screenHalfSize-vec2(1.0, 1.0);		
	gl_Position = vec4(resPos, 0.0, 1.0);
}