attribute highp 	vec3 aVertexPosition;
attribute highp		vec2 aVertexTexCoord;
attribute highp		vec4 aMatrixIndices;
attribute highp		vec4 aMatrixWeights;

uniform highp vec4 uMatrixPalette[22 * 3];
uniform highp mat4 uMVPMatrix;

varying lowp vec2 fragTexCoord;

const int CONST_0 = 0;
const int CONST_1 = 1;
const int CONST_2 = 2;
const int CONST_3 = 3;

void skinPosition(const in vec4 position, const in float weight, const in int baseIndex, inout vec4 skinnedPosition) {
	vec4 temp;
	
	temp.x = dot(position, uMatrixPalette[baseIndex]);
	temp.y = dot(position, uMatrixPalette[baseIndex + CONST_1]);
	temp.z = dot(position, uMatrixPalette[baseIndex + CONST_2]);
	temp.w = position.w;
	
	skinnedPosition += temp * weight;
}

void main() {
	vec4 srcVertexPos  = vec4(aVertexPosition, 1.0);
	vec4 outVertexPos = vec4(0.0);
	
	// @todo Premultiply (* CONST_3) in C++ code!!! No need to multiply in shader!!!
	skinPosition(srcVertexPos, aMatrixWeights[0], int(aMatrixIndices[0]), outVertexPos);
	skinPosition(srcVertexPos, aMatrixWeights[1], int(aMatrixIndices[1]), outVertexPos);
	skinPosition(srcVertexPos, aMatrixWeights[2], int(aMatrixIndices[2]), outVertexPos);
	skinPosition(srcVertexPos, aMatrixWeights[3], int(aMatrixIndices[3]), outVertexPos);
	
	gl_Position = uMVPMatrix * outVertexPos;
	fragTexCoord = aVertexTexCoord;
}

